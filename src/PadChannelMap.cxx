// @Siyuan.Sun@cern.ch

#include "NSWSTGCMapping/PadChannelMap.h"

using namespace std;

PadChannelMap::PadChannelMap(){
  Init();
  verbose = false;
}

void PadChannelMap::SetVerbose(bool v) {
  verbose = v;
}

void PadChannelMap::Init() {

  std::map<std::pair<int, int>, std::pair<int,int>> ichan_map;

  for (uint ivmm=1; ivmm < 3; ivmm++ ){
    for( uint ichan = 0; ichan< 64; ichan++ ) {

      ichan_map.insert( std::make_pair(std::make_pair(ivmm, ichan), std::make_pair(-1,-1)) );

    }
  }

  DetectorPadMapping.insert(std::make_pair("QS1P1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS1P2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS1P3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS1P4", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS2P1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS2P2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS2P3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS2P4", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS3P1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS3P2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS3P3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS3P4", ichan_map));
  
  DetectorPadMapping.insert(std::make_pair("QL1P1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL1P2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL1P3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL1P4", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL2P1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL2P2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL2P3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL2P4", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL3P1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL3P2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL3P3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL3P4", ichan_map));
  
  DetectorPadMapping.insert(std::make_pair("QS1C1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS1C2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS1C3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS1C4", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS2C1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS2C2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS2C3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS2C4", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS3C1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS3C2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS3C3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QS3C4", ichan_map));
  
  DetectorPadMapping.insert(std::make_pair("QL1C1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL1C2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL1C3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL1C4", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL2C1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL2C2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL2C3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL2C4", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL3C1", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL3C2", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL3C3", ichan_map));
  DetectorPadMapping.insert(std::make_pair("QL3C4", ichan_map));
  
  //------------------------------------------------------//

  DetectorNPadsPerRow.insert( std::make_pair("QS1P1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS1P2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS1P3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS1P4", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS2P1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS2P2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS2P3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS2P4", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS3P1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS3P2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS3P3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS3P4", -1) );

  DetectorNPadsPerRow.insert( std::make_pair("QL1P1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL1P2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL1P3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL1P4", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL2P1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL2P2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL2P3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL2P4", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL3P1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL3P2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL3P3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL3P4", -1) );

  //------------------------------------------------------//

  DetectorNPadsPerRow.insert( std::make_pair("QS1C1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS1C2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS1C3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS1C4", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS2C1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS2C2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS2C3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS2C4", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS3C1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS3C2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS3C3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QS3C4", -1) );

  DetectorNPadsPerRow.insert( std::make_pair("QL1C1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL1C2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL1C3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL1C4", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL2C1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL2C2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL2C3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL2C4", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL3C1", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL3C2", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL3C3", -1) );
  DetectorNPadsPerRow.insert( std::make_pair("QL3C4", -1) );

  //------------------------------------------------------//

  DetectorNPadsTotal.insert( std::make_pair("QS1P1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS1P2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS1P3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS1P4", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS2P1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS2P2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS2P3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS2P4", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS3P1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS3P2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS3P3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS3P4", -1) );

  DetectorNPadsTotal.insert( std::make_pair("QL1P1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL1P2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL1P3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL1P4", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL2P1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL2P2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL2P3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL2P4", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL3P1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL3P2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL3P3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL3P4", -1) );

  //------------------------------------------------------//

  DetectorNPadsTotal.insert( std::make_pair("QS1C1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS1C2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS1C3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS1C4", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS2C1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS2C2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS2C3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS2C4", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS3C1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS3C2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS3C3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QS3C4", -1) );

  DetectorNPadsTotal.insert( std::make_pair("QL1C1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL1C2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL1C3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL1C4", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL2C1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL2C2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL2C3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL2C4", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL3C1", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL3C2", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL3C3", -1) );
  DetectorNPadsTotal.insert( std::make_pair("QL3C4", -1) );

}

void PadChannelMap::Clear() {

  for (uint ivmm=1; ivmm < 3; ivmm++ ){
    for( uint ichan = 0; ichan< 64; ichan++ ) {

      DetectorPadMapping["QS1P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS1P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS1P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS1P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS2P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS2P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS2P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS2P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS3P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS3P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS3P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS3P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);

      DetectorPadMapping["QL1P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL1P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL1P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL1P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL2P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL2P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL2P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL2P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL3P1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL3P2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL3P3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL3P4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);

      DetectorPadMapping["QS1C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS1C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS1C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS1C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS2C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS2C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS2C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS2C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS3C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS3C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS3C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QS3C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);

      DetectorPadMapping["QL1C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL1C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL1C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL1C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL2C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL2C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL2C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL2C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL3C1"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL3C2"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL3C3"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);
      DetectorPadMapping["QL3C4"][std::make_pair(ivmm, ichan)] = std::make_pair(-1, -1);

    }
  }

  DetectorNPadsPerRow["QS1P1"] = -1;
  DetectorNPadsPerRow["QS1P2"] = -1;
  DetectorNPadsPerRow["QS1P3"] = -1;
  DetectorNPadsPerRow["QS1P4"] = -1;
  DetectorNPadsPerRow["QS2P1"] = -1;
  DetectorNPadsPerRow["QS2P2"] = -1;
  DetectorNPadsPerRow["QS2P3"] = -1;
  DetectorNPadsPerRow["QS2P4"] = -1;
  DetectorNPadsPerRow["QS3P1"] = -1;
  DetectorNPadsPerRow["QS3P2"] = -1;
  DetectorNPadsPerRow["QS3P3"] = -1;
  DetectorNPadsPerRow["QS3P4"] = -1;

  DetectorNPadsPerRow["QL1P1"] = -1;
  DetectorNPadsPerRow["QL1P2"] = -1;
  DetectorNPadsPerRow["QL1P3"] = -1;
  DetectorNPadsPerRow["QL1P4"] = -1;
  DetectorNPadsPerRow["QL2P1"] = -1;
  DetectorNPadsPerRow["QL2P2"] = -1;
  DetectorNPadsPerRow["QL2P3"] = -1;
  DetectorNPadsPerRow["QL2P4"] = -1;
  DetectorNPadsPerRow["QL3P1"] = -1;
  DetectorNPadsPerRow["QL3P2"] = -1;
  DetectorNPadsPerRow["QL3P3"] = -1;
  DetectorNPadsPerRow["QL3P4"] = -1;

  DetectorNPadsPerRow["QS1C1"] = -1;
  DetectorNPadsPerRow["QS1C2"] = -1;
  DetectorNPadsPerRow["QS1C3"] = -1;
  DetectorNPadsPerRow["QS1C4"] = -1;
  DetectorNPadsPerRow["QS2C1"] = -1;
  DetectorNPadsPerRow["QS2C2"] = -1;
  DetectorNPadsPerRow["QS2C3"] = -1;
  DetectorNPadsPerRow["QS2C4"] = -1;
  DetectorNPadsPerRow["QS3C1"] = -1;
  DetectorNPadsPerRow["QS3C2"] = -1;
  DetectorNPadsPerRow["QS3C3"] = -1;
  DetectorNPadsPerRow["QS3C4"] = -1;

  DetectorNPadsPerRow["QL1C1"] = -1;
  DetectorNPadsPerRow["QL1C2"] = -1;
  DetectorNPadsPerRow["QL1C3"] = -1;
  DetectorNPadsPerRow["QL1C4"] = -1;
  DetectorNPadsPerRow["QL2C1"] = -1;
  DetectorNPadsPerRow["QL2C2"] = -1;
  DetectorNPadsPerRow["QL2C3"] = -1;
  DetectorNPadsPerRow["QL2C4"] = -1;
  DetectorNPadsPerRow["QL3C1"] = -1;
  DetectorNPadsPerRow["QL3C2"] = -1;
  DetectorNPadsPerRow["QL3C3"] = -1;
  DetectorNPadsPerRow["QL3C4"] = -1;

  //------------------------------//

  DetectorNPadsTotal["QS1P1"] = -1;
  DetectorNPadsTotal["QS1P2"] = -1;
  DetectorNPadsTotal["QS1P3"] = -1;
  DetectorNPadsTotal["QS1P4"] = -1;
  DetectorNPadsTotal["QS2P1"] = -1;
  DetectorNPadsTotal["QS2P2"] = -1;
  DetectorNPadsTotal["QS2P3"] = -1;
  DetectorNPadsTotal["QS2P4"] = -1;
  DetectorNPadsTotal["QS3P1"] = -1;
  DetectorNPadsTotal["QS3P2"] = -1;
  DetectorNPadsTotal["QS3P3"] = -1;
  DetectorNPadsTotal["QS3P4"] = -1;

  DetectorNPadsTotal["QL1P1"] = -1;
  DetectorNPadsTotal["QL1P2"] = -1;
  DetectorNPadsTotal["QL1P3"] = -1;
  DetectorNPadsTotal["QL1P4"] = -1;
  DetectorNPadsTotal["QL2P1"] = -1;
  DetectorNPadsTotal["QL2P2"] = -1;
  DetectorNPadsTotal["QL2P3"] = -1;
  DetectorNPadsTotal["QL2P4"] = -1;
  DetectorNPadsTotal["QL3P1"] = -1;
  DetectorNPadsTotal["QL3P2"] = -1;
  DetectorNPadsTotal["QL3P3"] = -1;
  DetectorNPadsTotal["QL3P4"] = -1;

  DetectorNPadsTotal["QS1C1"] = -1;
  DetectorNPadsTotal["QS1C2"] = -1;
  DetectorNPadsTotal["QS1C3"] = -1;
  DetectorNPadsTotal["QS1C4"] = -1;
  DetectorNPadsTotal["QS2C1"] = -1;
  DetectorNPadsTotal["QS2C2"] = -1;
  DetectorNPadsTotal["QS2C3"] = -1;
  DetectorNPadsTotal["QS2C4"] = -1;
  DetectorNPadsTotal["QS3C1"] = -1;
  DetectorNPadsTotal["QS3C2"] = -1;
  DetectorNPadsTotal["QS3C3"] = -1;
  DetectorNPadsTotal["QS3C4"] = -1;

  DetectorNPadsTotal["QL1C1"] = -1;
  DetectorNPadsTotal["QL1C2"] = -1;
  DetectorNPadsTotal["QL1C3"] = -1;
  DetectorNPadsTotal["QL1C4"] = -1;
  DetectorNPadsTotal["QL2C1"] = -1;
  DetectorNPadsTotal["QL2C2"] = -1;
  DetectorNPadsTotal["QL2C3"] = -1;
  DetectorNPadsTotal["QL2C4"] = -1;
  DetectorNPadsTotal["QL3C1"] = -1;
  DetectorNPadsTotal["QL3C2"] = -1;
  DetectorNPadsTotal["QL3C3"] = -1;
  DetectorNPadsTotal["QL3C4"] = -1;

}

bool PadChannelMap::SetNPadChannelMap( bool isSmall, bool isPivot,
				       int quadNum, int layerNum,
				       int n_tot_pads, int n_pads_per_row ) {

  bool is_okay = true;
  
  //-------------------------------------------------------------//                                                                                                                                     
  //      Swap 1 & 4, 2 & 3 layer number for confirm wedge                                                                                                                                              
  //   Because Benoit saves gas gap number and not layer number                                                                                                                                         
  //-------------------------------------------------------------// 
  
  if(isPivot == false){
      if      ( layerNum == 1 ) layerNum = 4;
      else if ( layerNum == 2 ) layerNum = 3;
      else if ( layerNum == 3 ) layerNum = 2;
      else if ( layerNum == 4 ) layerNum = 1;
      else {
          std::cout << "FATAL: No such layer number : " << layerNum << std::endl;
      }
  }
  //-----------------------------------------------//
  //-----------------------------------------------//

  if ( !( quadNum >= 1 && quadNum <= 3 ) ) {
    is_okay = false;
  }

  if ( !( layerNum >= 1 && layerNum <= 4 ) ) {
    is_okay = false;
  }

  //---------------------------------------------//

  if ( !( n_tot_pads >= 1 ) ) {
    is_okay = false;
  }

  if ( !(n_pads_per_row >= 1 ) ) {
    is_okay = false;
  }

  //--------------------------------------------//

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorNPadsPerRow.find(m_sx.str()) != DetectorNPadsPerRow.end() ) {
    DetectorNPadsPerRow[m_sx.str()] = n_pads_per_row;
  }
  if ( DetectorNPadsTotal.find(m_sx.str()) != DetectorNPadsTotal.end() ) {
    DetectorNPadsTotal [m_sx.str()] = n_tot_pads;
  }

  return is_okay;

}

bool PadChannelMap::SetPadChannelMap( int ith_vmm,  int ith_vmm_chan,
				      bool isSmall, bool isPivot,
				      int quadNum, int layerNum, 
				      int padNumConstruction, int padNumAssembly ) {
  
  bool is_okay = true;

  int ichan = -1;

  //----------------------------------------------//

  int n_tot_pads;
  is_okay = ReturnNPadsTotal( isSmall, isPivot,
			      quadNum, layerNum,
			      n_tot_pads );


  int n_pads_per_row;
  is_okay = ReturnNPadPerRow( isSmall, isPivot,
			      quadNum, layerNum,
			      n_pads_per_row );

  //-----------------------------------------------//

  if ( !( ith_vmm >= 1 && ith_vmm <= 2 ) ) {
    is_okay = false;
  }

  if      ( ith_vmm_chan >=  0 ) ichan = ith_vmm_chan % 64;

  if ( !( ichan >= 0 && ichan <= 63 ) ) {
    is_okay = false;
  }

  //-----------------------------------------------//
  
  if ( !( quadNum >= 1 && quadNum <= 3 ) ) {
    is_okay = false;
  }

  if ( !( layerNum >= 1 && layerNum <= 4 ) ) {
    is_okay = false;
  }

  //---------------------------------------------//

  if ( !( padNumConstruction >= 1 && 
	  padNumConstruction <= n_tot_pads ) && 
       padNumConstruction != -1 ) {
    is_okay = false;
  }
  if ( !( padNumAssembly >= 1 && 
	  padNumAssembly <= n_tot_pads ) && 
       padNumAssembly != -1 ) {
    is_okay = false;
  }

  //--------------------------------------------//

  if ( !is_okay ) {
    std::cout << "Mapping Failed" << std::endl;
    std::cout << "ivmm " << ith_vmm << " ichan " << ith_vmm_chan 
	      << " Quad " << quadNum << " Layer " << layerNum 
	      << " Pad Number (Construction Convention) " << padNumConstruction 
	      << " Pad Number (Assembly Convention) " << padNumAssembly
	      << " n_tot_pads " << n_tot_pads << " n_pad_per_row " << n_pads_per_row << std::endl;
  }
  else {
    
    //---------------------------------------------------//

    if ( verbose ) {
      std::cout << "Setting Pad Channel: ivmm " << ith_vmm << " ith_vmm_chan " << ichan;
      if ( isSmall ) std::cout << " QS ";
      else           std::cout << " QL ";
      std::cout << quadNum;
      if ( isPivot ) std::cout << " P ";
      else           std::cout << " C ";
      std::cout << layerNum;
      std::cout << " pad num (Construction Convention) " << padNumConstruction 
		<< " pad num (Assembly Convention) " << padNumAssembly 
		<< " n_pad_per_row " << n_pads_per_row << std::endl;
    }

    //----------------------------------------------------//

    m_sx.str("");
    if ( isSmall ) m_sx << "QS" << quadNum;
    else           m_sx << "QL" << quadNum;
    if ( isPivot ) m_sx << "P"  << layerNum;
    else           m_sx << "C"  << layerNum;

    if ( DetectorPadMapping.find(m_sx.str()) != DetectorPadMapping.end() ) {
      DetectorPadMapping [m_sx.str()][std::make_pair(ith_vmm, ichan)] = std::make_pair(padNumConstruction, padNumAssembly);
    }
    else {
      std::cout << m_sx.str().c_str() << " not found " << std::endl;
    }

    //--------------------------------------------------//
  }

  //------------------------------------------//

  return is_okay;

}

bool PadChannelMap::ReturnNPadsTotal( bool isSmall, bool isPivot,
                                      int quadNum, int layerNum,
                                      int &n_tot_pads ) {

  bool found = false;

  n_tot_pads = -1;

  m_sx.str("");
  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorNPadsTotal.find(m_sx.str()) != DetectorNPadsTotal.end() ) {
    if ( verbose ) std::cout << m_sx.str().c_str() << " found " << std::endl;
    n_tot_pads = DetectorNPadsTotal[m_sx.str()];
    found = true;
  }

  return found;

}

bool PadChannelMap::ReturnNPadPerRow( bool isSmall, bool isPivot,
				      int quadNum, int layerNum,
				      int &n_pads_per_row ) {

  bool found = false;

  n_pads_per_row = -1;

  m_sx.str("");
  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorNPadsPerRow.find(m_sx.str()) != DetectorNPadsPerRow.end() ) {
    if ( verbose ) std::cout << m_sx.str().c_str() << " found " << std::endl;
    n_pads_per_row = DetectorNPadsPerRow[m_sx.str()];
    found = true;
  }

  return found;

}

bool PadChannelMap::ReturnPadChannelNumber( int ith_vmm,  int ith_vmm_chan,
					    bool isSmall, bool isPivot,
					    int quadNum, int layerNum, 
					    int &padNumConstruction, int &padNumAssembly ) {

  bool foundNum = false;

  std::map<std::pair<int,int>, std::pair<int,int>> ichan_map;

  m_sx.str("");
  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  if ( DetectorPadMapping.find(m_sx.str()) != DetectorPadMapping.end() ) {

    if ( verbose ) std::cout << m_sx.str().c_str() << " found " << std::endl;

    ichan_map = DetectorPadMapping[m_sx.str()] ;

    padNumConstruction = ichan_map[ std::make_pair(ith_vmm, ith_vmm_chan) ].first;
    padNumAssembly     = ichan_map[ std::make_pair(ith_vmm, ith_vmm_chan) ].second;

    foundNum = true;

  }

  return ( foundNum );

}

bool PadChannelMap::ReturnPadChannelNumber( int ith_vmm,  int ith_vmm_chan,
                                            bool isSmall, bool isPivot,
                                            int quadNum,  int layerNum,
                                            int &padXConstruction,    int &padYConstruction,    
					    int &padXAssembly,        int &padYAssembly,
					    int &n_pads_per_row ) {


  int ipad_construction = -1;
  int ipad_assembly = -1;
  int num_pads_per_row = -1;

  bool found = ReturnPadChannelNumber( ith_vmm,  ith_vmm_chan,
				       isSmall,  isPivot,
				       quadNum,  layerNum,
				       ipad_construction,
				       ipad_assembly);

  bool foundNperRow = ReturnNPadPerRow( isSmall,  isPivot,
					quadNum,  layerNum,
					num_pads_per_row );

  n_pads_per_row = num_pads_per_row;

  padXConstruction = ipad_construction % n_pads_per_row + 1;
  padYConstruction = ipad_construction / n_pads_per_row + 1;

  padXAssembly = ipad_assembly % n_pads_per_row + 1;
  padYAssembly = ipad_assembly / n_pads_per_row + 1;

  return ( found && foundNperRow );

}

bool PadChannelMap::ReturnElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
                                                                 bool isSmall, bool isPivot,
                                                                 int quadNum, int layerNum,
                                                                 int  padNumConstruction,
                                                                 int &padNumAssembly ) {

  bool found = false;

  std::map<std::pair<int,int>, std::pair<int,int>> ichan_map;

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  for ( int ivmm=0; ivmm < 8; ivmm++ ) {
    for ( int ich=0; ich < 64; ich++ ) {

      int ipad_construction = -1;
      int ipad_assembly = -1;

      bool ifound = ReturnPadChannelNumber( ivmm, ich,
                                            isSmall, isPivot,
                                            quadNum, layerNum,
                                            ipad_construction,
                                            ipad_assembly);

      if ( verbose ) {
	std::cout << ipad_construction << " " << padNumConstruction << std::endl;
      }

      if ( ifound && ( ipad_construction == padNumConstruction ) ) {
        ith_vmm = ivmm;
        ith_vmm_chan = ich;
        padNumAssembly = ipad_assembly;
        found = true;
        break;
      }
    } // loop over chan                                                                                                                                                                           

    if ( found ) break;

  }

  if ( !found ) {
    ith_vmm = -1;
    ith_vmm_chan = -1;
    padNumAssembly = -1;
  }

  return found;

}

bool PadChannelMap::ReturnElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
							     bool isSmall, bool isPivot,
							     int quadNum, int layerNum, 
							     int &padNumConstruction,
							     int  padNumAssembly ) {
  
  bool found = false;

  std::map<std::pair<int,int>, std::pair<int,int>> ichan_map;

  m_sx.str("");

  if ( isSmall ) m_sx << "QS" << quadNum;
  else           m_sx << "QL" << quadNum;
  if ( isPivot ) m_sx << "P"  << layerNum;
  else           m_sx << "C"  << layerNum;

  for ( int ivmm=0; ivmm < 8; ivmm++ ) {
    for ( int ich=0; ich < 64; ich++ ) {
      
      int ipad_construction = -1;
      int ipad_assembly = -1;

      bool ifound = ReturnPadChannelNumber( ivmm, ich,
					    isSmall, isPivot,
					    quadNum, layerNum,
					    ipad_construction,
					    ipad_assembly);

      if ( verbose ) {
	std::cout << ipad_assembly << " " << padNumAssembly << std::endl;
      }

      if ( ifound && ( ipad_assembly == padNumAssembly ) ) {
	ith_vmm = ivmm;
	ith_vmm_chan = ich;
	padNumConstruction = ipad_construction;
	found = true;
	break;
      }
    } // loop over chan

    if ( found ) break;

  }

  if ( !found ) {
    ith_vmm = -1;
    ith_vmm_chan = -1;
    padNumConstruction = -1;
  }

  return found;

}

bool PadChannelMap::ReturnElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
								 bool isSmall, bool isPivot,
								 int quadNum, int layerNum, 
								 int padXConstruction, int  padYConstruction,
								 int &padXAssembly,    int &padYAssembly) {

  bool found = false;

  int n_pads_per_row;

  found = ReturnNPadPerRow( isSmall, isPivot,
			    quadNum, layerNum,
			    n_pads_per_row );

  if ( !found ) {
    std::cout << "Error: cannot find N pad per row " << std::endl;
    return found;
  }

  int padNumConstruction = (padXConstruction-1) + (padYConstruction-1)*n_pads_per_row;
  int padNumAssembly     = -1;

  //-----------------------------------------------//

  int ivmm  = -1;
  int ichan = -1;

  found = ReturnElectronicsChannelNumber_Construction( ivmm,    ichan,
						       isSmall, isPivot,
						       quadNum, layerNum, 
						       padNumConstruction, 
						       padNumAssembly);

  padXAssembly = padNumAssembly%n_pads_per_row + 1;
  padYAssembly = padNumAssembly/n_pads_per_row + 1;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return found;

}

bool PadChannelMap::ReturnElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
							     bool isSmall, bool isPivot,
							     int quadNum, int layerNum,
							     int &padXConstruction, int &padYConstruction,
							     int  padXAssembly,     int  padYAssembly) {

  bool found = false;

  int n_pads_per_row;

  found = ReturnNPadPerRow( isSmall, isPivot,
			    quadNum, layerNum,
			    n_pads_per_row );

  if ( !found ) {
    std::cout << "Error: cannot find N pad per row " << std::endl;
    return found;
  }

  int padNumAssembly     = (padXAssembly-1) + (padYAssembly-1)*n_pads_per_row;
  int padNumConstruction = -1;

  //-----------------------------------------------//                                                                                                                                             

  int ivmm  = -1;
  int ichan = -1;

  found = ReturnElectronicsChannelNumber_Assembly( ivmm,    ichan,
						   isSmall, isPivot,
						   quadNum, layerNum,
						   padNumConstruction,
						   padNumAssembly);

  padXConstruction = padNumConstruction%n_pads_per_row + 1;
  padYConstruction = padNumConstruction/n_pads_per_row + 1;

  ith_vmm      = ivmm;
  ith_vmm_chan = ichan;

  return found;

}
