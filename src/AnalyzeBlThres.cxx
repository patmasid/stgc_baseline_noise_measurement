#define AnalyzeBlThres_cxx

#include <iostream>
#include <vector>
#include <cstring>
#include "NSWSTGCMapping/AnalyzeBlThres.h"
#include "NSWSTGCMapping/ABMapping.h"
#include <cmath>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <algorithm>

#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <array>

#include <iomanip>
#include "THStack.h"
#include <math.h>
#include <TLine.h>
#include "TPad.h"
#include "TApplication.h"
#include <TSystem.h>
#include "TAxis.h"
#include <fstream>
#include <cstdint>
//#include <filesystem>                                                                                                                                                                          
#include <TLegend.h>
#include <sys/stat.h>
#include "TProfile.h"
#include "TMath.h"
#include <TFrame.h>
#include <TGaxis.h>

#include <set>
#include <numeric>

void AnalyzeBlThres::SetVerbose(bool v){
  
  m_Verbose = v;

}

AnalyzeBlThres::AnalyzeBlThres(const char *inputFileSumm, const char *inputFile, const char *inputHistFile, const char *outFileName_webpage, const char *outFileName_extra, const char* outputFilePath, const char* SorL, const char* PorC){

    m_Verbose = false;

    oFile_web = new TFile(outFileName_webpage, "recreate");
    
    oFile_extra = new TFile(outFileName_extra, "recreate");

    outputFilePath_ = outputFilePath;

    std::string outputTxt_wPhysicalChanPlots = outputFilePath_ + "output_ProbChan_wPhysicalChanPlots.txt";
    
    std::string outputTxt_wUnConnChanRef = outputFilePath_ + "output_ProbChan_wUnConnChanRef.txt";
    
    std::string outputTxt_wPhysicalChanPlots_AbsRef = outputFilePath_ + "output_ProbChan_wPhysicalChanPlots_AbsRef.txt";

    std::string output_baseline_outside_150_200_mV = outputFilePath_ + "baseline_outside_150_200mV_2.txt";

    std::string output_baseline_outside_150_200_mV_PhysStripPad = outputFilePath_ + "baseline_outside_150_200mV_PhysStripPad.txt";

    m_strOutput_Phys.open(outputTxt_wPhysicalChanPlots.c_str());

    m_strOutput_Elec.open(outputTxt_wUnConnChanRef.c_str());

    m_strOutput_Phys_AbsRef.open(outputTxt_wPhysicalChanPlots_AbsRef.c_str());

    m_strOutput_baslines.open(output_baseline_outside_150_200_mV.c_str());

    m_strOutput_baslines_PhysStripPad.open(output_baseline_outside_150_200_mV_PhysStripPad.c_str());

    s_SorL = SorL;
    s_PorC = PorC;

    if(s_SorL=="S") isSmall=true;
    else if(s_SorL=="L") isSmall=false;
    else std::cout<<"Please provide S or L as third argument"<<std::endl;
    
    std::cout << SorL << " " << s_SorL << " " << isSmall << std::endl;

    if(s_PorC=="P") isPivot=true;
    else if(s_PorC=="C") isPivot=false;
    else std::cout<<"Please provide P or C as fourth argument"<<std::endl;

    std::cout << PorC << " " << s_PorC << " " << isPivot << std::endl;

    baseline_Summary = new TTree("Bl_Summary","Bl_Summary");

    if( ! baseline_Summary->ReadFile(inputFileSumm, "data/C:sTGC_Board:vmm_str/C:vmm_id/D:channel_str/C:channel_id/D:mean_str/C:mean/D:stdev_str/C:stdev/D:median_str/C:median/D", ' ') ){
        std::cerr << "[Error] Failed to read the txtfile." << std::endl;
    }

    baseline_Summary->Print();

    baseline_Summary->SetBranchAddress("sTGC_Board", &boardname);
    baseline_Summary->SetBranchAddress("vmm_id", &vmmidnum);
    baseline_Summary->SetBranchAddress("channel_id", &channelidnum);
    baseline_Summary->SetBranchAddress("mean", &meannum);
    baseline_Summary->SetBranchAddress("stdev", &stdevnum);
    baseline_Summary->SetBranchAddress("median", &mediannum);
    
    baseline = new TTree("Bl_all","Bl_all");

    if( ! baseline->ReadFile(inputFile, "data/C:sTGC_Board_bl:vmm_id_bl/D:channel_id_bl:tpdac_bl:random1_bl:random2_bl:baseline_bl", ' ') ){
        std::cerr << "[Error] Failed to read the txtfile." << std::endl;
    }

    baseline->Print();

    baseline->SetBranchAddress("sTGC_Board_bl", &boardname_bl);
    baseline->SetBranchAddress("vmm_id_bl", &vmmidnum_bl);
    baseline->SetBranchAddress("channel_id_bl", &channelidnum_bl);
    baseline->SetBranchAddress("tpdac_bl", &tpdac_bl);
    baseline->SetBranchAddress("random1_bl", &random1_bl);
    baseline->SetBranchAddress("random2_bl", &random2_bl);
    baseline->SetBranchAddress("baseline_bl", &baseline_bl);

    iHistFile = TFile::Open(inputHistFile);

    std::cout<<"Construction Ended "<<std::endl;

}

void AnalyzeBlThres::InitMapHistograms(){
    
  total_quads = 3;
  total_layers = 4;
  total_types_FEBs = 2;
  total_vmms_per_sFEB = 8;
  total_vmms_per_pFEB = 3;
  total_chans_pervmm = 64;
  
  m_ABMap = new ABMapping();
  m_ABMap->SetVerbose(0);
  m_ABMap->LoadBenoitMapping();
  
  total_PhysicalChannels_Strip.resize(total_quads);
  total_PhysicalChannels_Wire_S.resize(total_quads);
  total_PhysicalChannels_Pad_S.resize(total_quads);
  
  h_Strip_Stdev_perLayer_perQuad.resize(total_quads);
  v_Strip_median_rms_perlayer_perQuad.resize(total_quads);
  h_Pad_Stdev_perLayer_perQuad.resize(total_quads);
  v_Pad_median_rms_perlayer_perQuad.resize(total_quads);
  
  l_Strip.resize(total_quads);
  l_Pad.resize(total_quads);

  t_Legend.resize(total_quads);
  
  box_Pad.resize(total_quads);
  
  S_line_median.resize(total_quads);
  S_line_halfmedian.resize(total_quads);
  S_line_twicemedian.resize(total_quads);
  
  P_line_median.resize(total_quads);
  P_line_halfmedian.resize(total_quads);
  P_line_twicemedian.resize(total_quads);
  
  for(int iQ=0; iQ<total_quads; iQ++){
    
    total_PhysicalChannels_Wire_S[iQ].resize(total_layers);
    total_PhysicalChannels_Pad_S[iQ].resize(total_layers);
    
    h_Strip_Stdev_perLayer_perQuad[iQ].resize(total_layers);
    v_Strip_median_rms_perlayer_perQuad[iQ].resize(total_layers);
    h_Pad_Stdev_perLayer_perQuad[iQ].resize(total_layers);
    v_Pad_median_rms_perlayer_perQuad[iQ].resize(total_layers);
    
    l_Strip[iQ].resize(total_layers);
    l_Pad[iQ].resize(total_layers);
    
    t_Legend[iQ].resize(total_layers);

    box_Pad[iQ].resize(total_layers);
    
    S_line_median[iQ].resize(total_layers);
    S_line_halfmedian[iQ].resize(total_layers);
    S_line_twicemedian[iQ].resize(total_layers);
    
    P_line_median[iQ].resize(total_layers);
    P_line_halfmedian[iQ].resize(total_layers);
    P_line_twicemedian[iQ].resize(total_layers);
    
    for(int iL=0; iL<total_layers; iL++){
      
      int num_Strips, num_Wires, num_Pads;
      
      if( !(m_ABMap->ReturnNStripsTotal(isSmall, isPivot, iQ+1, iL+1, num_Strips)) ) std::cerr<<"Cannot determine no of strips for given Quad "<< iQ+1 << " and given layer " << iL+1 << std::endl;
      if( !(m_ABMap->ReturnNWiresTotal(isSmall, isPivot, iQ+1, iL+1, num_Wires)) ) std::cerr<<"Cannot determine no of wires for given Quad "<< iQ+1 << " and given layer " <<iL+1 <<std::endl;
      if( !(m_ABMap->ReturnNPadsTotal(isSmall, isPivot, iQ+1, iL+1, num_Pads)) ) std::cerr<<"Cannot determine no of pads for given Quad "<< iQ+1 << " and given layer " <<iL+1 <<std::endl;
      
      total_PhysicalChannels_Strip[iQ] = num_Strips;
      
      total_PhysicalChannels_Wire_S[iQ][iL] = num_Wires;
      
      total_PhysicalChannels_Pad_S[iQ][iL] = num_Pads; 

      std::cout << "Quad: " << iQ+1 << " Layer: " << iL+1 << " num strips: " << num_Strips << std::endl;
      std::cout << "Quad: " << iQ+1 << " Layer: " << iL+1 << " num wires: " << num_Wires << std::endl;
      std::cout << "Quad: " << iQ+1 << " Layer: " << iL+1 << " num pads: " << num_Pads << std::endl;
     
      t_Legend[iQ][iL].resize(total_types_FEBs);
 
    }
    
  }
  
  h_Strip_Stdev_perLayer.resize(total_layers);
  c_Strip_Stdev_perLayer.resize(total_layers);

  h_Strip_Stdev_perLayer_TH1.resize(total_layers);

  h_Wire_Stdev_perLayer.resize(total_layers);
  c_Wire_Stdev_perLayer.resize(total_layers);
  h_Pad_Stdev_perLayer.resize(total_layers);
  c_Pad_Stdev_perLayer.resize(total_layers);
  
  Strip_leg.resize(total_layers);
  Wire_leg.resize(total_layers);
  Pad_leg.resize(total_layers);
  
  Strip_h_1D_perLayer.resize(total_layers);
  Wire_h_1D_perLayer.resize(total_layers);
  Pad_h_1D_perLayer.resize(total_layers);
  
  v_Strip_median_rms_perlayer.resize(total_layers);
  v_Pad_median_rms_perlayer.resize(total_layers);
  
  v_Strip_lowNoiseChannels.resize(total_layers);
  v_Pad_lowNoiseChannels.resize(total_layers);
  v_lowNoiseElecChannels.resize(total_layers);
  
  v_Strip_lowNoiseChannels_AbsRef.resize(total_layers);
  v_Pad_lowNoiseChannels_AbsRef.resize(total_layers);
  v_lowNoiseElecChannels_AbsRef.resize(total_layers);

  v_Strip_highNoiseChannels.resize(total_layers);
  v_Pad_highNoiseChannels.resize(total_layers);
  v_highNoiseElecChannels.resize(total_layers);
  
  v_Strip_lowNoiseChannels_UnConnChanRef.resize(total_layers);
  v_Pad_lowNoiseChannels_UnConnChanRef.resize(total_layers);
  v_lowNoiseElecChannels_UnConnChanRef.resize(total_layers);
  
  StripLayer_ChannelAlivenessRating.resize(total_layers);
  PadLayer_ChannelAlivenessRating.resize(total_layers);
  
  W_myline1.resize(total_layers);
  W_myline2.resize(total_layers);
  
  for(int iL=0; iL<total_layers; iL++){
    v_Strip_lowNoiseChannels[iL].resize(total_quads);
    v_Pad_lowNoiseChannels[iL].resize(total_quads);
    v_lowNoiseElecChannels[iL].resize(total_quads);
    
    v_Strip_lowNoiseChannels_AbsRef[iL].resize(total_quads);
    v_Pad_lowNoiseChannels_AbsRef[iL].resize(total_quads);
    v_lowNoiseElecChannels_AbsRef[iL].resize(total_quads);

    v_Strip_highNoiseChannels[iL].resize(total_quads);
    v_Pad_highNoiseChannels[iL].resize(total_quads);
    v_highNoiseElecChannels[iL].resize(total_quads);
    
    v_Strip_lowNoiseChannels_UnConnChanRef[iL].resize(total_quads);
    v_Pad_lowNoiseChannels_UnConnChanRef[iL].resize(total_quads);
    v_lowNoiseElecChannels_UnConnChanRef[iL].resize(total_quads);
    
    for(int iQ=0; iQ<total_quads; iQ++){
      
      v_lowNoiseElecChannels[iL][iQ].resize(total_types_FEBs);
      v_lowNoiseElecChannels_AbsRef[iL][iQ].resize(total_types_FEBs);

      v_highNoiseElecChannels[iL][iQ].resize(total_types_FEBs);
      v_lowNoiseElecChannels_UnConnChanRef[iL][iQ].resize(total_types_FEBs);
      
    }
  }
  
  h_baseline_pdo.resize(total_quads);
  h_baseline_mV.resize(total_quads);
  h_baseline_query.resize(total_quads);
  h_baseline_query_vmm.resize(total_quads);
  c_baseline_pdo.resize(total_quads);
  c_baseline_mV.resize(total_quads);
  c_baseline_query.resize(total_quads);
  c_baseline_query_vmm.resize(total_quads);
  
  h_Stdev_perFEB_ElecChan.resize(total_layers);
  c_Stdev_perFEB_ElecChan.resize(total_layers);

  quadDirName.resize(total_quads);
  LayerDirName.resize(total_quads);
  febTypeDirName.resize(total_quads);
  VMMDirName.resize(total_quads);
  Baseline_pdoDirName.resize(total_quads);
  Baseline_mVDirName.resize(total_quads);
  Baseline_queryDirName.resize(total_quads);
  Stdev_perFEBDirName.resize(total_quads);
  
  for(int iQ=0; iQ<total_quads; iQ++){
    
    h_baseline_pdo[iQ].resize(total_layers);
    h_baseline_mV[iQ].resize(total_layers);
    h_baseline_query[iQ].resize(total_layers);
    h_baseline_query_vmm[iQ].resize(total_layers);
    c_baseline_pdo[iQ].resize(total_layers);
    c_baseline_mV[iQ].resize(total_layers);
    c_baseline_query[iQ].resize(total_layers);
    c_baseline_query_vmm[iQ].resize(total_layers);
    
    h_Stdev_perFEB_ElecChan[iQ].resize(total_layers);
    c_Stdev_perFEB_ElecChan[iQ].resize(total_layers);
    
    LayerDirName[iQ].resize(total_layers);
    febTypeDirName[iQ].resize(total_layers);
    VMMDirName[iQ].resize(total_layers);
    Baseline_pdoDirName[iQ].resize(total_layers);
    Baseline_mVDirName[iQ].resize(total_layers);
    Baseline_queryDirName[iQ].resize(total_layers);
    Stdev_perFEBDirName[iQ].resize(total_layers);
    
    for(int iL=0; iL<total_layers; iL++){
      
      h_baseline_pdo[iQ][iL].resize(total_types_FEBs);
      h_baseline_mV[iQ][iL].resize(total_types_FEBs);
      h_baseline_query[iQ][iL].resize(total_types_FEBs);
      h_baseline_query_vmm[iQ][iL].resize(total_types_FEBs);
      c_baseline_pdo[iQ][iL].resize(total_types_FEBs);
      c_baseline_mV[iQ][iL].resize(total_types_FEBs);
      c_baseline_query[iQ][iL].resize(total_types_FEBs);
      c_baseline_query_vmm[iQ][iL].resize(total_types_FEBs);
      
      h_Stdev_perFEB_ElecChan[iQ][iL].resize(total_types_FEBs);
      c_Stdev_perFEB_ElecChan[iQ][iL].resize(total_types_FEBs);
      
      febTypeDirName[iQ][iL].resize(total_types_FEBs);
      VMMDirName[iQ][iL].resize(total_types_FEBs);
      Baseline_pdoDirName[iQ][iL].resize(total_types_FEBs);
      Baseline_mVDirName[iQ][iL].resize(total_types_FEBs);
      Baseline_queryDirName[iQ][iL].resize(total_types_FEBs);
      Stdev_perFEBDirName[iQ][iL].resize(total_types_FEBs);
      
      for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
	
	std::string FEBType;
	int numVMMs = -1;
	
	if(iispFEB==0) {FEBType = "sFEB"; numVMMs=total_vmms_per_sFEB;}
	else if(iispFEB==1) {FEBType = "pFEB"; numVMMs=total_vmms_per_pFEB;}
	else std::cout<<"Invalid value for ispFEB"<<std::endl;
	
	h_baseline_pdo[iQ][iL][iispFEB].resize(numVMMs);
	h_baseline_mV[iQ][iL][iispFEB].resize(numVMMs);
	h_baseline_query[iQ][iL][iispFEB].resize(numVMMs);
	h_baseline_query_vmm[iQ][iL][iispFEB].resize(numVMMs);
	c_baseline_pdo[iQ][iL][iispFEB].resize(numVMMs);
	c_baseline_mV[iQ][iL][iispFEB].resize(numVMMs);
	c_baseline_query[iQ][iL][iispFEB].resize(numVMMs);
	c_baseline_query_vmm[iQ][iL][iispFEB].resize(numVMMs);
        
	VMMDirName[iQ][iL][iispFEB].resize(numVMMs);
	Baseline_pdoDirName[iQ][iL][iispFEB].resize(numVMMs);
	Baseline_mVDirName[iQ][iL][iispFEB].resize(numVMMs);
	Baseline_queryDirName[iQ][iL][iispFEB].resize(numVMMs);
	
	for(int iVMM=0; iVMM<numVMMs; iVMM++){
	  
	  h_baseline_pdo[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
	  h_baseline_mV[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
	  h_baseline_query[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
	  c_baseline_pdo[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
	  c_baseline_mV[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
	  c_baseline_query[iQ][iL][iispFEB][iVMM].resize(total_chans_pervmm);
          
	}
	
      }
      
    }
    
  }


  std::string name_Strip_Stdev_perlayer, name_Wire_Stdev_perlayer, name_Pad_Stdev_perlayer, name_Strip_Stdev_perQuadperlayer, name_Pad_Stdev_perQuadperlayer;
  
  for(int iL=0; iL<total_layers; iL++){
    
    //============= Strip
    
    name_Strip_Stdev_perlayer = "Strip_Stdev_all_Quads_Layer_"+std::to_string(iL+1);
    
    h_Strip_Stdev_perLayer[iL] = new TH2F(name_Strip_Stdev_perlayer.c_str(),name_Strip_Stdev_perlayer.c_str(),total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2],0.5,total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2]+0.5,200000,0,2000);
    h_Strip_Stdev_perLayer[iL]->SetDirectory(0);
    
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBit(TAxis::kLabelsHori);
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(1,"1");
    
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0],std::to_string(total_PhysicalChannels_Strip[0]).c_str());
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1],std::to_string(total_PhysicalChannels_Strip[1]).c_str());
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2],std::to_string(total_PhysicalChannels_Strip[2]).c_str());
    
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(100,"100");
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(200,"200");
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(300,"300");
    
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+100,"100");
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+200,"200");
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+300,"300");
    
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+100,"100");
    h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+200,"200");
    if(!isSmall){
      h_Strip_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+300,"300");
    }
    
    h_Strip_Stdev_perLayer_TH1[iL] = new TH1F(name_Strip_Stdev_perlayer.c_str(),name_Strip_Stdev_perlayer.c_str(),total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2],0.5,total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2]+0.5);
    h_Strip_Stdev_perLayer_TH1[iL]->SetDirectory(0);

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBit(TAxis::kLabelsHori);
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(1,"1");

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0],std::to_string(total_PhysicalChannels_Strip[0]).c_str());
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1],std::to_string(total_PhysicalChannels_Strip[1]).c_str());
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2],std::to_string(total_PhysicalChannels_Strip[2]).c_str());

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(100,"100");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(200,"200");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(300,"300");

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+100,"100");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+200,"200");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+300,"300");

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+100,"100");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+200,"200");
    if(!isSmall){
      h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+300,"300");
    }

    c_Strip_Stdev_perLayer[iL] = new TCanvas(name_Strip_Stdev_perlayer.c_str(),name_Strip_Stdev_perlayer.c_str(), 1200, 800);
    
    //============ Wire
    
    name_Wire_Stdev_perlayer = "Wire_Stdev_all_Quads_Layer_"+std::to_string(iL+1);
    
    std::string Wire_Q1, Wire_Q2, Wire_Q3;
    
    Wire_Q1 = std::to_string(total_PhysicalChannels_Wire_S[0][iL]);
    Wire_Q2 = std::to_string(total_PhysicalChannels_Wire_S[1][iL]);
    Wire_Q3 = std::to_string(total_PhysicalChannels_Wire_S[2][iL]);
    
    h_Wire_Stdev_perLayer[iL] = new TH2F(name_Wire_Stdev_perlayer.c_str(),name_Wire_Stdev_perlayer.c_str(),total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+total_PhysicalChannels_Wire_S[2][iL],0.5,total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+total_PhysicalChannels_Wire_S[2][iL]+0.5,200000,0,2000);
    h_Wire_Stdev_perLayer[iL]->SetDirectory(0);
    
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBit(TAxis::kLabelsHori);
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(1,"1");
    
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL],Wire_Q1.c_str());
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL], Wire_Q2.c_str());
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+total_PhysicalChannels_Wire_S[2][iL], Wire_Q3.c_str());
    
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(10,"10");
    
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+10,"10");
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+20,"20");
    
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+10,"10");
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+20,"20");
    h_Wire_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+30,"30");
    
    c_Wire_Stdev_perLayer[iL] = new TCanvas(name_Wire_Stdev_perlayer.c_str(),name_Wire_Stdev_perlayer.c_str(), 1000, 800);
    
    //============ Pad                                                                                                                                
    std::string Pad_Q1, Pad_Q2, Pad_Q3;
    
    Pad_Q1 = std::to_string(total_PhysicalChannels_Pad_S[0][iL]);
    Pad_Q2 = std::to_string(total_PhysicalChannels_Pad_S[1][iL]);
    Pad_Q3 = std::to_string(total_PhysicalChannels_Pad_S[2][iL]);
    
    name_Pad_Stdev_perlayer = "Pad_Stdev_all_Quads_Layer_"+std::to_string(iL+1);
    
    h_Pad_Stdev_perLayer[iL] = new TH2F(name_Pad_Stdev_perlayer.c_str(),name_Pad_Stdev_perlayer.c_str(),total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+total_PhysicalChannels_Pad_S[2][iL],0.5,total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+total_PhysicalChannels_Pad_S[2][iL]+0.5,200000,0,2000);
    h_Pad_Stdev_perLayer[iL]->SetDirectory(0);
    
    h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBit(TAxis::kLabelsHori);
    h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(1,"1");
    h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL], Pad_Q1.c_str());
    h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL], Pad_Q2.c_str());
    h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+total_PhysicalChannels_Pad_S[2][iL], Pad_Q3.c_str());
    
    for(int iQ=0; iQ<total_quads; iQ++){
      
      name_Strip_Stdev_perQuadperlayer = "Strip_Stdev_Quad"+std::to_string(iQ+1)+"_Layer"+std::to_string(iL+1);
      
      h_Strip_Stdev_perLayer_perQuad[iQ][iL] = new TH2F(name_Strip_Stdev_perQuadperlayer.c_str(),name_Strip_Stdev_perQuadperlayer.c_str(),total_PhysicalChannels_Strip[iQ],0.5,total_PhysicalChannels_Strip[iQ]+0.5,200000,0,2000);
      h_Strip_Stdev_perLayer_perQuad[iQ][iL]->SetDirectory(0);
      
      name_Pad_Stdev_perQuadperlayer = "Pad_Stdev_Quad"+std::to_string(iQ+1)+"_Layer"+std::to_string(iL+1);
      
      h_Pad_Stdev_perLayer_perQuad[iQ][iL] = new TH2F(name_Pad_Stdev_perQuadperlayer.c_str(),name_Pad_Stdev_perQuadperlayer.c_str(),total_PhysicalChannels_Pad_S[iQ][iL],0.5,total_PhysicalChannels_Pad_S[iQ][iL]+0.5,200000,0,2000);
      h_Pad_Stdev_perLayer_perQuad[iQ][iL]->SetDirectory(0);
      
    }

    if(iL==0 || iL==1){

      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(20,"20");
      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(40,"40");
      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(60,"60");
      if(!isSmall){
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(80,"80");
      }
      
      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+20,"20");
      if(isSmall == true && isPivot == false){
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+40,"40");
      }
      else if(isSmall == false && isPivot == true){
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+40,"40");
      }
      else if(isSmall == false && isPivot == false){
	
      }

      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+20,"20");
      if(isSmall == true && isPivot == false){

      }
      else if(isSmall == false && isPivot == true){
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+40,"40");
      }
      else if(isSmall == false && isPivot == false){
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+40,"40");
      }

    }
    
    else if(iL==2 || iL==3){
      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(20,"20");
      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(40,"40");
      if(isSmall == true && isPivot == false){
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(60,"60");
      }
      else if(isSmall == false && isPivot == true){
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(80,"80");
      }
      else if(isSmall == false && isPivot == false){

      }

      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+20,"20");
      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+40,"40");
      if(isSmall == true && isPivot == false){

      }
      else if(isSmall == false && isPivot == true){
	h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+60,"60");
      }
      else if(isSmall == false && isPivot == false){

      }

      h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+20,"20");
      if(isSmall == true && isPivot == false){

      }
      else if(isSmall == false && isPivot == true){
	h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+40,"40");
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+60,"60");
      }
      else if(isSmall == false && isPivot == false){
        h_Pad_Stdev_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+40,"40");
      }

    }
    
    c_Pad_Stdev_perLayer[iL] = new TCanvas(name_Pad_Stdev_perlayer.c_str(),name_Pad_Stdev_perlayer.c_str(), 1000, 800);
    
  }
  
  for(int iQ=0; iQ<total_quads; iQ++){
    
    for(int iL=0; iL<total_layers; iL++){
      
      for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
	
	std::string FEBType;
	int numVMMs = -1;
	
	if(iispFEB==0) {FEBType = "sFEB"; numVMMs=total_vmms_per_sFEB;}
	else if(iispFEB==1) {FEBType = "pFEB"; numVMMs=total_vmms_per_pFEB;}
	else std::cout<<"Invalid value for ispFEB"<<std::endl;
	
	std::string name_h_baseline_stdev, name_c_baseline_stdev;
        
	name_h_baseline_stdev = "BaselineStdev_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType;
        
	if(iispFEB==0){
	  h_Stdev_perFEB_ElecChan[iQ][iL][iispFEB] = new TH2F(name_h_baseline_stdev.c_str(), name_h_baseline_stdev.c_str(), 512, -0.5, 511.5, 10000, 0, 100);
	}
	else if(iispFEB==1){
	  h_Stdev_perFEB_ElecChan[iQ][iL][iispFEB] = new TH2F(name_h_baseline_stdev.c_str(), name_h_baseline_stdev.c_str(), 192, -0.5, 191.5, 10000, 0, 100);
	}
	name_c_baseline_stdev ="c_BaselineStdev_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType;
	c_Stdev_perFEB_ElecChan[iQ][iL][iispFEB] = new TCanvas(name_c_baseline_stdev.c_str(), name_c_baseline_stdev.c_str(), 800, 800);
	
	for(int iVMM=0; iVMM<numVMMs; iVMM++){
	  
	  std::string name_h_baseline_query_vmm, name_c_baseline_query_vmm;
          
	  name_h_baseline_query_vmm = "BaselineQuery_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_VMM"+std::to_string(iVMM);
	  h_baseline_query_vmm[iQ][iL][iispFEB][iVMM] = new TH2F(name_h_baseline_query_vmm.c_str(), name_h_baseline_query_vmm.c_str(), 100, 0.5, 100.5, 1024, -0.5, 1023.5);
	  h_baseline_query_vmm[iQ][iL][iispFEB][iVMM]->SetDirectory(0);
	  name_c_baseline_query_vmm = "c_BaselineQuery_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_VMM"+std::to_string(iVMM);
	  c_baseline_query_vmm[iQ][iL][iispFEB][iVMM] = new TCanvas(name_c_baseline_query_vmm.c_str(), name_c_baseline_query_vmm.c_str(), 800, 800);
	  
	  for(int iChan=0; iChan<total_chans_pervmm; iChan++){
	    
	    std::string name_h_baseline_pdo, name_h_baseline_mV, name_h_baseline_query, name_c_baseline_pdo, name_c_baseline_mV, name_c_baseline_query;
	    
	    name_h_baseline_pdo = "BaselinePdo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_VMM"+std::to_string(iVMM)+"_Chan"+std::to_string(iChan);
	    h_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan] = new TH1F(name_h_baseline_pdo.c_str(), name_h_baseline_pdo.c_str(), 1024, -0.5, 1023.5);
	    h_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan]->SetDirectory(0);
	    name_c_baseline_pdo = "c_BaselinePdo_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_VMM"+std::to_string(iVMM)+"_Chan"+std::to_string(iChan);
	    c_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan] = new TCanvas(name_c_baseline_pdo.c_str(), name_c_baseline_pdo.c_str(), 800, 800);
            
	    name_h_baseline_mV = "BaselineMV_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_VMM"+std::to_string(iVMM)+"_Chan"+std::to_string(iChan);
	    h_baseline_mV[iQ][iL][iispFEB][iVMM][iChan] = new TH1F(name_h_baseline_mV.c_str(), name_h_baseline_mV.c_str(), 40000, -0.5, 400);
	    h_baseline_mV[iQ][iL][iispFEB][iVMM][iChan]->SetDirectory(0);
	    name_c_baseline_mV = "c_BaselineMV_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_VMM"+std::to_string(iVMM)+"_Chan"+std::to_string(iChan);
	    c_baseline_mV[iQ][iL][iispFEB][iVMM][iChan] = new TCanvas(name_c_baseline_mV.c_str(), name_c_baseline_mV.c_str(), 800, 800);
	    
	    name_h_baseline_query = "BaselineQuery_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_VMM"+std::to_string(iVMM)+"_Chan"+std::to_string(iChan);
	    h_baseline_query[iQ][iL][iispFEB][iVMM][iChan] = new TH2F(name_h_baseline_query.c_str(), name_h_baseline_query.c_str(), 100, 0.5, 100.5, 1024, -0.5, 1023.5);
	    h_baseline_query[iQ][iL][iispFEB][iVMM][iChan]->SetDirectory(0);
	    name_c_baseline_query = "c_BaselineQuery_Q"+std::to_string(iQ+1)+"_L"+std::to_string(iL+1)+"_"+FEBType+"_VMM"+std::to_string(iVMM)+"_Chan"+std::to_string(iChan);
	    c_baseline_query[iQ][iL][iispFEB][iVMM][iChan] = new TCanvas(name_c_baseline_query.c_str(), name_c_baseline_query.c_str(), 800, 800);
	    
	  }
	}
	
      }
    }
  }
  
}

void AnalyzeBlThres::CreateHistDir(){
    
  for(int iQ=0; iQ<total_quads; iQ++){
    
    quadDirName[iQ] = "Quad_"+std::to_string(iQ+1)+"/";
    oFile_extra->mkdir(quadDirName[iQ].c_str());
    
    for(int iL=0; iL<total_layers; iL++){
      
      LayerDirName[iQ][iL] = quadDirName[iQ]+"Layer_"+std::to_string(iL+1)+"/";
      oFile_extra->mkdir(LayerDirName[iQ][iL].c_str());
      
      for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
	
	std::string FEBType;
	int numVMMs = -1;
	
	if(iispFEB==0) {FEBType = "sFEB"; numVMMs=8;}
	else if(iispFEB==1) {FEBType = "pFEB"; numVMMs=3;}
	else std::cout<<"Invalid value for ispFEB"<<std::endl;
	
	febTypeDirName[iQ][iL][iispFEB] = LayerDirName[iQ][iL]+FEBType+"/";
	oFile_extra->mkdir(febTypeDirName[iQ][iL][iispFEB].c_str());
	
	Stdev_perFEBDirName[iQ][iL][iispFEB] = febTypeDirName[iQ][iL][iispFEB]+"bl_stdev/";
	oFile_extra->mkdir(Stdev_perFEBDirName[iQ][iL][iispFEB].c_str());
        
	for(int iVMM=0; iVMM<numVMMs; iVMM++){
	  
	  VMMDirName[iQ][iL][iispFEB][iVMM] = febTypeDirName[iQ][iL][iispFEB]+"VMM"+std::to_string(iVMM)+"/";
	  oFile_extra->mkdir(VMMDirName[iQ][iL][iispFEB][iVMM].c_str());
	  
	  Baseline_pdoDirName[iQ][iL][iispFEB][iVMM] = VMMDirName[iQ][iL][iispFEB][iVMM]+"Baseline_pdo/";
	  oFile_extra->mkdir(Baseline_pdoDirName[iQ][iL][iispFEB][iVMM].c_str());
          
	  Baseline_mVDirName[iQ][iL][iispFEB][iVMM] = VMMDirName[iQ][iL][iispFEB][iVMM]+"Baseline_mV/";
	  oFile_extra->mkdir(Baseline_mVDirName[iQ][iL][iispFEB][iVMM].c_str());
	  
	  Baseline_queryDirName[iQ][iL][iispFEB][iVMM] = VMMDirName[iQ][iL][iispFEB][iVMM]+"Baseline_query/";
	  oFile_extra->mkdir(Baseline_queryDirName[iQ][iL][iispFEB][iVMM].c_str());
	  
	}
	
      }
      
    }
    
  }
  
}

void AnalyzeBlThres::EventLoop_Summary() {
  
  if (baseline_Summary == 0) {std::cout << "empty tree" << std::endl; return;}
  
  Long64_t nentries = baseline_Summary->GetEntriesFast();
  std::cout << "nentries " << nentries << std::endl;
  
  Long64_t nbytes = 0, nb = 0;
  int decade = 0;
  
  //========================== Defining variables ============================//                                                                                                                         
  int event_number=0;
  
  //========================== Variables for TGraph ============================//                                                                                                                          
  // loop over entries of the tree                                                                                                                                                                   
  for (Long64_t jentry=0; jentry<nentries; jentry++) {
    
    //=== print fraction of total events processed ====                                                                                                                                         
    double progress = 10.0 * jentry / (1.0 * nentries);
    int k = int (progress);
    if (k > decade) std::cout << 10 * k << " %" << std::endl;
    decade = k;
    
    //===== read this entry =====//                                                                                                                                                                    
    Long64_t ientry = baseline_Summary->LoadTree(jentry);
    if (ientry < 0) break;
    nb = baseline_Summary->GetEntry(jentry);   nbytes += nb;
    
    
    event_number+=1;
    
    int layer=-1, quad=-1, ispFEB=-1, vmm=-1, channel=-1;
    
    vmm = vmmidnum;
    channel = channelidnum;
    
    std::string s, FEB, layerquadstring;
    std::stringstream ss;
    
    ss << boardname;
    ss >> s;
    
    std::ifstream ssin(s.c_str());

    if(s.find("SFEB") != std::string::npos) ispFEB = 0;
    else if(s.find("PFEB") != std::string::npos) ispFEB = 1;
    else std::cerr << "The board name should have 'SFEB' or 'PFEB' in it. " << std::endl;

    if(s.find("L1") != std::string::npos) layer = 1;
    else if(s.find("L2") != std::string::npos) layer = 2;
    else if(s.find("L3") != std::string::npos) layer = 3;
    else if(s.find("L4") != std::string::npos) layer = 4;
    else std::cerr << "The board name should have 'L' with the layer number in it. (e.g. L1)" <<std::endl;

    if(s.find("Q1") != std::string::npos) quad = 1;
    else if(s.find("Q2") != std::string::npos) quad = 2;
    else if(s.find("Q3") != std::string::npos) quad = 3;
    else std::cerr << "The board name should have 'L' with the layer number in it. (e.g. Q1) " <<std::endl;

    //std::cout<<"ispFEB "<<ispFEB<<" layer "<<layer<<" quad "<<quad<<std::endl;
    
    std::string line;
    
    //Fill the txt file for baselines which are not in the range 150 to 200 mv. 
    
    double mean_mV = meannum*1000.0/4095.0;
    double stdev_mV = stdevnum*1000.0/4095.0;
    double median_mV = mediannum*1000.0/4095.0;
    
    if(mean_mV < 150 || mean_mV > 200){
      m_strOutput_baslines << "BASELINE_OUTSIDE_150_200_MV " 
			   << s << " " 
			   << " vmm " << vmmidnum
			   << " channel " << channelidnum
			   << " mean " << mean_mV
			   << " stdev " << stdev_mV
			   << " median " << median_mV
			   << std::endl;
      if(ispFEB==0){
        bool isStrip = false;
        isStrip = m_ABMap->IsConnected_sFEB( vmmidnum, channelidnum, isSmall, isPivot, quad, layer );
        if(isStrip) {
          m_strOutput_baslines_PhysStripPad << "BASELINE_OUTSIDE_150_200_MV "
					    << s << " "
					    << " vmm " << vmmidnum
					    << " channel " << channelidnum
					    << " mean " << mean_mV
					    << " stdev " << stdev_mV
					    << " median " << median_mV
					    << std::endl;
        }
      }
      else if(ispFEB==1 && (vmm==1 || vmm==2) ){
        bool isPad = false;
        isPad = m_ABMap->IsConnected_pFEB( vmmidnum, channelidnum, isSmall, isPivot, quad, layer );
        if(isPad) {
          m_strOutput_baslines_PhysStripPad << "BASELINE_OUTSIDE_150_200_MV "
                                            << s << " "
                                            << " vmm " << vmmidnum
                                            << " channel " << channelidnum
                                            << " mean " << mean_mV
                                            << " stdev " << stdev_mV
                                            << " median " << median_mV
                                            << std::endl;
        }
      }
    }
    
    //std::cout<<s<<std::endl;
    
    h_Stdev_perFEB_ElecChan[quad-1][layer-1][ispFEB]->Fill(vmm*64+channel, stdevnum);
    
    int stripNoConstruct=-1, stripNoAssem=-1, wireNoConstruct=-1, wireNoAssem=-1, padNoConstruct=-1, padNoAssem=-1;
    if(ispFEB==0){
      //std::cout<<"blahhhhhhhh"<<std::endl;
      if(m_ABMap->ReturnStripChannelNumber(vmmidnum,channel,isSmall,isPivot,quad,layer,stripNoConstruct,stripNoAssem)){
	if( !(stripNoConstruct==-1 || stripNoAssem==-1) ){
	  if(quad==1){
	    h_Strip_Stdev_perLayer[layer-1]->Fill(stripNoAssem,stdevnum*1000.0/4095.0);
	    h_Strip_Stdev_perLayer_TH1[layer-1]->SetBinContent(stripNoAssem,stdevnum*1000.0/4095.0);
	  }
	  else if(quad==2){
	    h_Strip_Stdev_perLayer[layer-1]->Fill(stripNoAssem+total_PhysicalChannels_Strip[0],stdevnum*1000.0/4095.0);
	    h_Strip_Stdev_perLayer_TH1[layer-1]->SetBinContent(stripNoAssem+total_PhysicalChannels_Strip[0],stdevnum*1000.0/4095.0);
	  }
	  else if(quad==3){
	    h_Strip_Stdev_perLayer[layer-1]->Fill(stripNoAssem+total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1],stdevnum*1000.0/4095.0);
	    h_Strip_Stdev_perLayer_TH1[layer-1]->SetBinContent(stripNoAssem+total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1],stdevnum*1000.0/4095.0);
	  }
	  h_Strip_Stdev_perLayer_perQuad[quad-1][layer-1]->Fill(stripNoAssem,stdevnum*1000.0/4095.0);
	}
        
      }
      else{
	std::cout<<"Something wrong with the Strip channel to Board channel info!"<<std::endl;
	std::cout<<"Vmm real id "<<vmmidnum<<" vmm channel "<<channel<<" isSmall "<<1<<" isPivot "<<1<<" quad "<<quad<<" layer "<<layer<<std::endl;
      }
    }
    else if(ispFEB==1){
      if(m_ABMap->ReturnWireChannelNumber(vmmidnum,channel,isSmall,isPivot,quad,layer,wireNoConstruct,wireNoAssem) && vmmidnum==0){
	if( !(wireNoConstruct==-1 || wireNoAssem==-1) ){
	  if(quad==1) h_Wire_Stdev_perLayer[layer-1]->Fill(wireNoAssem,stdevnum*1000.0/4095.0);
	  else if(quad==2) h_Wire_Stdev_perLayer[layer-1]->Fill(wireNoAssem+total_PhysicalChannels_Wire_S[0][layer-1],stdevnum*1000.0/4095.0);
	  else if(quad==3) h_Wire_Stdev_perLayer[layer-1]->Fill(wireNoAssem+total_PhysicalChannels_Wire_S[0][layer-1]+total_PhysicalChannels_Wire_S[1][layer-1],stdevnum*1000.0/4095.0);
	  //else if(quad==3) h_Wire_Stdev_perLayer[layer-1]->Fill(wireNoAssem+771,stdevnum*1000.0/4095.0);
	}
      }
      else if(m_ABMap->ReturnPadChannelNumber(vmmidnum,channel,isSmall,isPivot,quad,layer,padNoConstruct, padNoAssem) && (vmmidnum==1 || vmmidnum==2) ){
	if( !(padNoConstruct==-1 || padNoAssem==-1) ){
	  
	  if(quad==1) h_Pad_Stdev_perLayer[layer-1]->Fill(padNoAssem,stdevnum*1000.0/4095.0);
	  else if(quad==2) h_Pad_Stdev_perLayer[layer-1]->Fill(padNoAssem+total_PhysicalChannels_Pad_S[0][layer-1],stdevnum*1000.0/4095.0);
	  else if(quad==3) h_Pad_Stdev_perLayer[layer-1]->Fill(padNoAssem+total_PhysicalChannels_Pad_S[0][layer-1]+total_PhysicalChannels_Pad_S[1][layer-1],stdevnum*1000.0/4095.0);
	  h_Pad_Stdev_perLayer_perQuad[quad-1][layer-1]->Fill(padNoAssem,stdevnum*1000.0/4095.0);
          
	}
      }
      else{
	std::cout<<"Something wrong with the Wire/Pad channel to Board channel info!"<<std::endl;
        
      }
    }
    
  }
  
  m_strOutput_baslines.close();
  
}

void AnalyzeBlThres::EventLoop() {

    if (baseline == 0) {std::cout << "empty tree" << std::endl; return;}

    Long64_t nentries = baseline->GetEntriesFast();
    std::cout << "nentries " << nentries << std::endl;

    Long64_t nbytes = 0, nb = 0;
    int decade = 0;

    //========================== Defining variables ============================//                                                                                                                         
    int event_number=0;

    //========================== Variables for TGraph ============================//                                                                                    
    // loop over entries of the tree                                                                                                                           
    
    for (Long64_t jentry=0; jentry<nentries; jentry++) {

        //=== print fraction of total events processed ====                                                                                                             
        double progress = 10.0 * jentry / (1.0 * nentries);
        int k = int (progress);
        if (k > decade) std::cout << 10 * k << " %" << std::endl;
        decade = k;

        //===== read this entry =====//                                                                                                                                                                    
        Long64_t ientry = baseline->LoadTree(jentry);
        if (ientry < 0) break;
        nb = baseline->GetEntry(jentry);   nbytes += nb;


        event_number+=1;

        int layer=-1, quad=-1, ispFEB=-1, vmm=-1, channel=-1;

        vmm = vmmidnum_bl;
        channel = channelidnum_bl;

        std::string s, FEB, layerquadstring;
        std::stringstream ss;

        ss << boardname_bl;
        ss >> s;

        std::ifstream ssin(s.c_str());

        std::string line;
     
	if(s.find("SFEB") != std::string::npos) ispFEB = 0;
        else if(s.find("PFEB") != std::string::npos) ispFEB = 1;
        else std::cerr << "The board name should have 'SFEB' or 'PFEB' in it. " << std::endl;

        if(s.find("L1") != std::string::npos) layer = 1;
        else if(s.find("L2") != std::string::npos) layer = 2;
        else if(s.find("L3") != std::string::npos) layer = 3;
        else if(s.find("L4") != std::string::npos) layer = 4;
        else std::cerr << "The board name should have 'L' with the layer number in it. (e.g. L1)" <<std::endl;

        if(s.find("Q1") != std::string::npos) quad = 1;
        else if(s.find("Q2") != std::string::npos) quad = 2;
        else if(s.find("Q3") != std::string::npos) quad = 3;
        else std::cerr << "The board name should have 'L' with the layer number in it. (e.g. Q1) " <<std::endl;

        h_baseline_pdo[quad-1][layer-1][ispFEB][vmm][channel]->Fill(baseline_bl);
        h_baseline_mV[quad-1][layer-1][ispFEB][vmm][channel]->Fill(baseline_bl/4.095);
        int EntryNum = h_baseline_mV[quad-1][layer-1][ispFEB][vmm][channel]->GetEntries();
        h_baseline_query[quad-1][layer-1][ispFEB][vmm][channel]->Fill(EntryNum, baseline_bl);
        h_baseline_query_vmm[quad-1][layer-1][ispFEB][vmm]->Fill(EntryNum, baseline_bl);

    }
        

}



void AnalyzeBlThres::FindLowHighNoiseChannels_wPhysicalChanPlot(){

    //===== For strip                                                                                                                                                                                       

  for(int iL=0; iL<total_layers; iL++){
    
    for(int iQ=0; iQ<total_quads; iQ++){
      
      TProfile * Strip_h_1D = h_Strip_Stdev_perLayer_perQuad[iQ][iL]->ProfileX("Strip_h_1D",      1, -1, "");
      
      double median_h_Strip = TMath::Median(Strip_h_1D->GetSize(), Strip_h_1D->GetArray());
      double rms_h_Strip = Strip_h_1D->GetRMS(2);
      
      std::cout<<"median "<<median_h_Strip<<" rms "<<rms_h_Strip<<std::endl;
      
      std::pair<double,double> pairMedianRMS;
      pairMedianRMS = std::make_pair(median_h_Strip, rms_h_Strip);
      
      v_Strip_median_rms_perlayer_perQuad[iQ][iL]=pairMedianRMS;
      
      for(int iBin=1; iBin<=h_Strip_Stdev_perLayer_perQuad[iQ][iL]->GetNbinsX(); iBin++){
	
	//if( (Strip_h_1D->GetBinContent(iBin) < median_h_Strip) && ((median_h_Strip-Strip_h_1D->GetBinContent(iBin)) > 3*rms_h_Strip) ){                        
	
	int vmmid, channelid, stripConstruct;
	
	bool isElecChan = m_ABMap->ReturnStripElectronicsChannelNumber_Assembly( vmmid, channelid, isSmall, isPivot, iQ+1, iL+1, stripConstruct, iBin);
	if(!isElecChan) {std::cout << "Cannot convert Physical Strip channel number to Electronic Channel number" <<std::endl; continue;}
	
	if( (Strip_h_1D->GetBinContent(iBin) <= 0.5*median_h_Strip) ){
	  
	  v_Strip_lowNoiseChannels[iL][iQ].push_back(iBin);
	  
	  if(isElecChan) v_lowNoiseElecChannels[iL][iQ][0].push_back(std::make_pair(vmmid,channelid));
	  
	}
	
	else if( (Strip_h_1D->GetBinContent(iBin) > 2*median_h_Strip) ){
	  
	  v_Strip_highNoiseChannels[iL][iQ].push_back(iBin);
	  
	  if(isElecChan) v_highNoiseElecChannels[iL][iQ][0].push_back(std::make_pair(vmmid,channelid));
	  
	}
	
      }
      
    }
    
  }
  
  //===== For Pad                                                                                                                                                                                         
  
  for(int iL=0; iL<total_layers; iL++){
    
    for(int iQ=0; iQ<total_quads; iQ++){
      
      TProfile * Pad_h_1D = h_Pad_Stdev_perLayer_perQuad[iQ][iL]->ProfileX("Pad_h_1D",      1, -1, "");
      
      double median_h_Pad = TMath::Median(Pad_h_1D->GetSize(), Pad_h_1D->GetArray());;
      double rms_h_Pad = Pad_h_1D->GetRMS(2);
      
      std::cout<<"median "<<median_h_Pad<<" rms "<<rms_h_Pad<<std::endl;
      
      std::pair<double,double> pairMedianRMS;
      pairMedianRMS = std::make_pair(median_h_Pad, rms_h_Pad);
      
      v_Pad_median_rms_perlayer_perQuad[iQ][iL]=pairMedianRMS;
      
      for(int iBin=1; iBin<=h_Pad_Stdev_perLayer_perQuad[iQ][iL]->GetNbinsX(); iBin++){
	
	//if( (Pad_h_1D->GetBinContent(iBin) < median_h_Pad) && ((median_h_Pad-Pad_h_1D->GetBinContent(iBin)) > 3*rms_h_Pad) ){
	
	int vmmid, channelid, padConstruct;
	
	bool isElecChan = m_ABMap->ReturnPadElectronicsChannelNumber_Assembly( vmmid, channelid, isSmall, isPivot, iQ+1, iL+1, padConstruct, iBin);
	if(!isElecChan) {std::cout << "Cannot convert Physical Pad channel number to Electronic Channel number" <<std::endl; continue;}
	
	if( (Pad_h_1D->GetBinContent(iBin) <= 0.5*median_h_Pad) ){
	  
	  v_Pad_lowNoiseChannels[iL][iQ].push_back(iBin);
          
	  if(isElecChan) v_lowNoiseElecChannels[iL][iQ][1].push_back(std::make_pair(vmmid,channelid));
	  
	}
        
	else if( (Pad_h_1D->GetBinContent(iBin) > 2*median_h_Pad) ){
	  
	  v_Pad_highNoiseChannels[iL][iQ].push_back(iBin);
	  
	  if(isElecChan) v_highNoiseElecChannels[iL][iQ][1].push_back(std::make_pair(vmmid,channelid));
	  
	}
	
      }
      
    }
    
  }
  
}

void AnalyzeBlThres::FindLowNoiseChannels_wUnconnChanRef(){
    
  for(int iQ=0; iQ<total_quads; iQ++){
    
    for(int iL=0; iL<total_layers; iL++){
      
      for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
	
	std::string FEBType;
	int numVMMs = -1;
	
	if(iispFEB==0) {FEBType = "SFEB"; numVMMs=total_vmms_per_sFEB;}
	else if(iispFEB==1) {FEBType = "PFEB"; numVMMs=total_vmms_per_pFEB;}
	else std::cout<<"Invalid value for ispFEB"<<std::endl;
	
	std::vector<double> baselineStdev_perBoard; 
	
	TProfile *h_only_stdev = h_Stdev_perFEB_ElecChan[iQ][iL][iispFEB]->ProfileX("h_only_stdev",      1, -1, "");
        
	//std::cout << "Histogram Entries: " << h_only_stdev->GetEntries() << std::endl;; 
	
        
	//=============== Going over unconnected channels
	
	for(int iVMM=0; iVMM<numVMMs; iVMM++){
	  
	  for(int iChan=0; iChan<total_chans_pervmm; iChan++){
	    
	    if(iispFEB==0){
	      
	      bool issFEBConn = m_ABMap->IsConnected_sFEB( iVMM, iChan, isSmall, isPivot, iQ+1, iL+1 );
	      if(!issFEBConn){
		//std::cout << h_only_stdev->GetBinContent(iVMM*64+iChan+1)/4.095 << std::endl;
		baselineStdev_perBoard.push_back(h_only_stdev->GetBinContent(iVMM*64+iChan+1));
	      }
	    }
            
	    else if(iispFEB==1){
	      
	      bool ispFEBConn = true;
	      if(iVMM == 1 || iVMM == 2){
		ispFEBConn = m_ABMap->IsConnected_pFEB( iVMM, iChan, isSmall, isPivot, iQ+1, iL+1 );
		if(!ispFEBConn){
		  //std::cout << "Q" << iQ << " L" << iL+1 << " Pad stdev values: " << h_only_stdev->GetBinContent(iVMM*64+iChan+1)/4.095 << " VMM "<< iVMM << " Channel " << iChan << std::endl;
		  baselineStdev_perBoard.push_back(h_only_stdev->GetBinContent(iVMM*64+iChan+1));
		}
		
	      }
	      
	    }
	    
	  }
	  
	}
	
	size_t n = baselineStdev_perBoard.size() / 2;
	std::nth_element(baselineStdev_perBoard.begin(), baselineStdev_perBoard.begin()+n, baselineStdev_perBoard.end());
	double median_blStdev = baselineStdev_perBoard[n];
	
	size_t n2 = baselineStdev_perBoard.size();
	std::nth_element(baselineStdev_perBoard.begin(), baselineStdev_perBoard.begin()+n2, baselineStdev_perBoard.end());
	double maxStdev = *max_element(baselineStdev_perBoard.begin(), baselineStdev_perBoard.end());

	double mean_blStdev = std::accumulate(baselineStdev_perBoard.begin(), baselineStdev_perBoard.end(), 0LL) / baselineStdev_perBoard.size();
	double sq_sum = std::inner_product(baselineStdev_perBoard.begin(), baselineStdev_perBoard.end(), baselineStdev_perBoard.begin(), 0.0);
	double stdev_blStdev = std::sqrt(sq_sum / baselineStdev_perBoard.size() - mean_blStdev * mean_blStdev);
        
	//std::cout << FEBType << ": " << max_Stdev/4.095 << std::endl;
	
	//================= Going over connected channels
        
	for(int iVMM=0; iVMM<numVMMs; iVMM++){
	  
	  for(int iChan=0; iChan<total_chans_pervmm; iChan++){
	    
	    if(iispFEB==0){
	      
	      bool issFEBConn = m_ABMap->IsConnected_sFEB( iVMM, iChan, isSmall, isPivot, iQ+1, iL+1 );
	      if(issFEBConn){
		
		//std::cout << "sFEB Connected channel!! " << std::endl;
		
		if(h_only_stdev->GetBinContent(iVMM*64+iChan+1) < maxStdev ) {
		  
		  int stripNoConstruct=-1, stripNoAssem=-1;
		  
		  if( !(m_ABMap->ReturnStripChannelNumber(iVMM,iChan,isSmall,isPivot,iQ+1,iL+1,stripNoConstruct,stripNoAssem)) ) continue;
                  
		  //std::cout << "Strip Channel: " << stripNoAssem << std::endl;
		  
		  if( (stripNoConstruct==-1 || stripNoAssem==-1) ) continue;
		  v_Strip_lowNoiseChannels_UnConnChanRef[iL][iQ].push_back(stripNoAssem);
		  std::pair<int,int> vmmchanpair = std::make_pair(iVMM, iChan);
		  v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][iispFEB].push_back(vmmchanpair);
		  
		}
                
	      }
	      
	    }
	    
	    else if(iispFEB==1){
	      
	      bool ispFEBConn = false;
	      if(iVMM == 1 || iVMM == 2){
		ispFEBConn = m_ABMap->IsConnected_pFEB( iVMM, iChan, isSmall, isPivot, iQ+1, iL+1 );
                
		if(ispFEBConn){
		  
		  //std::cout << "pFEB Connected channel!! " << std::endl;
                  
		  if(h_only_stdev->GetBinContent(iVMM*64+iChan+1) < maxStdev ) {
		    
		    int padNoConstruct=-1, padNoAssem=-1;
                    
		    if( !(m_ABMap->ReturnPadChannelNumber(iVMM,iChan,isSmall,isPivot,iQ+1,iL+1,padNoConstruct,padNoAssem)) ) continue;
		    if( (padNoConstruct==-1 || padNoAssem==-1) ) continue;
                    
		    //std::cout << "Pad Channel: " << padNoAssem << std::endl;
		    v_Pad_lowNoiseChannels_UnConnChanRef[iL][iQ].push_back(padNoAssem);
		    std::pair<int,int> vmmchanpair = std::make_pair(iVMM, iChan);
		    v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][iispFEB].push_back(vmmchanpair);
		    
		  }
		  
		}
		
	      }
	      
	    }
	    
	  }
	  
	}
	
      }
      
    }
    
  }
    
}


void AnalyzeBlThres::FindLowNoiseChannels_wAbsRef(){
  
  for(int iL=0; iL<total_layers; iL++){

    for(int iQ=0; iQ<total_quads; iQ++){

      TProfile * Strip_h_1D = h_Strip_Stdev_perLayer_perQuad[iQ][iL]->ProfileX("Strip_h_1D",      1, -1, "");

      for(int iBin=1; iBin<=h_Strip_Stdev_perLayer_perQuad[iQ][iL]->GetNbinsX(); iBin++){

        int vmmid, channelid, stripConstruct;

        bool isElecChan = m_ABMap->ReturnStripElectronicsChannelNumber_Assembly( vmmid, channelid, isSmall, isPivot, iQ+1, iL+1, stripConstruct, iBin);
        if(!isElecChan) {std::cout << "Cannot convert Physical Strip channel number to Electronic Channel number" <<std::endl; continue;}

        if( (Strip_h_1D->GetBinContent(iBin) <= 0.5) ){

          v_Strip_lowNoiseChannels_AbsRef[iL][iQ].push_back(iBin);
	  if(isElecChan) v_lowNoiseElecChannels_AbsRef[iL][iQ][0].push_back(std::make_pair(vmmid,channelid));

        }

      }
      
      TProfile * Pad_h_1D = h_Pad_Stdev_perLayer_perQuad[iQ][iL]->ProfileX("Pad_h_1D",      1, -1, "");

      for(int iBin=1; iBin<=h_Pad_Stdev_perLayer_perQuad[iQ][iL]->GetNbinsX(); iBin++){

        int vmmid, channelid, padConstruct;

        bool isElecChan = m_ABMap->ReturnPadElectronicsChannelNumber_Assembly( vmmid, channelid, isSmall, isPivot, iQ+1, iL+1, padConstruct, iBin);
        if(vmmid==1 || vmmid==2){
          if(!isElecChan) {std::cout << "Cannot convert Physical Pad channel number to Electronic Channel number" <<std::endl; continue;}
          if( (Pad_h_1D->GetBinContent(iBin) <= 0.8) ){

            v_Pad_lowNoiseChannels_AbsRef[iL][iQ].push_back(iBin);
            if(isElecChan) v_lowNoiseElecChannels_AbsRef[iL][iQ][1].push_back(std::make_pair(vmmid,channelid));
          }
        }

      }

    }

  }

}


void AnalyzeBlThres::SaveHistograms(){

  for(int iL=0; iL<total_layers; iL++){
    
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(1);
    
    //======================================== Strip
    
    std::string Perfect, Ok, Problematic;
    Perfect = "PERFECT";
    Ok = "OK";
    Problematic = "PROBLEMATIC";
    
    //###################################################################################// 
    /////////////////////// Strips histograms /////////////////////////////////////////////
    //###################################################################################// 
    
    if( (v_Strip_lowNoiseChannels[iL][0].size() + v_Strip_lowNoiseChannels[iL][1].size() + v_Strip_lowNoiseChannels[iL][2].size()) == 0) {
      c_Strip_Stdev_perLayer[iL]->SetFillColor(kGreen);
      c_Strip_Stdev_perLayer[iL]->SetFrameFillColor(10);
      StripLayer_ChannelAlivenessRating[iL]=Perfect;
    }
    else if( (v_Strip_lowNoiseChannels[iL][0].size() + v_Strip_lowNoiseChannels[iL][1].size() + v_Strip_lowNoiseChannels[iL][2].size()) <= 2) {
      c_Strip_Stdev_perLayer[iL]->SetFillColor(kYellow);
      c_Strip_Stdev_perLayer[iL]->SetFrameFillColor(10);
      StripLayer_ChannelAlivenessRating[iL]=Ok;
    }
    else {
      c_Strip_Stdev_perLayer[iL]->SetFillColor(kRed);
      c_Strip_Stdev_perLayer[iL]->SetFrameFillColor(10);
      StripLayer_ChannelAlivenessRating[iL]=Problematic;
    }
    
    c_Strip_Stdev_perLayer[iL]->cd();
    
    std::string name_Strip_h_1D = "Strip_h_1D_Layer"+std::to_string(iL+1);
    
    Strip_h_1D_perLayer[iL] = h_Strip_Stdev_perLayer[iL]->ProfileX(name_Strip_h_1D.c_str(),      1, -1, "");
    
    h_Strip_Stdev_perLayer_TH1[iL]->Draw("L");
    
    double Strip_maxYAxis = h_Strip_Stdev_perLayer_TH1[iL]->GetBinContent(h_Strip_Stdev_perLayer_TH1[iL]->GetMaximumBin())*1.5;
    
    h_Strip_Stdev_perLayer_TH1[iL]->GetYaxis()->SetRangeUser(0,Strip_maxYAxis);
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetTitleSize(0.045);
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetLabelSize(0.045);
    h_Strip_Stdev_perLayer_TH1[iL]->GetYaxis()->SetTitleSize(0.045);
    h_Strip_Stdev_perLayer_TH1[iL]->GetYaxis()->SetLabelSize(0.045);
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetTitleOffset(1);
    h_Strip_Stdev_perLayer_TH1[iL]->GetYaxis()->SetTitleOffset(1);
    h_Strip_Stdev_perLayer_TH1[iL]->SetMarkerSize(0.75);
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetTitle("Strip Physical Channel No.");
    h_Strip_Stdev_perLayer_TH1[iL]->GetYaxis()->SetTitle("Stdev on baseline ADC Sample [mV]");
    h_Strip_Stdev_perLayer_TH1[iL]->SetMarkerColor(kBlue);
    h_Strip_Stdev_perLayer_TH1[iL]->SetLineColor(kBlue);
    h_Strip_Stdev_perLayer_TH1[iL]->SetLineWidth(3);
    h_Strip_Stdev_perLayer_TH1[iL]->SetMarkerStyle(kFullSquare);
    
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBit(TAxis::kLabelsHori);
    
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(1,"1");

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0],std::to_string(total_PhysicalChannels_Strip[0]).c_str());
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1],std::to_string(total_PhysicalChannels_Strip[1]).c_str());
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+total_PhysicalChannels_Strip[2],std::to_string(total_PhysicalChannels_Strip[2]).c_str());

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(100,"100");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(200,"200");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(300,"300");

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+100,"100");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+200,"200");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+300,"300");

    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+100,"100");
    h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+200,"200");
    if(!isSmall){
      h_Strip_Stdev_perLayer_TH1[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+300,"300");
    }

    gPad->Update();

    //========== Drawing necessary lines ============//
    
    for(int iQ=0; iQ<total_quads; iQ++){
      
      std::pair< std::vector<int>, std::vector<int>> p_NumConnChan_VMMid = m_ABMap->NumChan_perVmm_PhysConnected(isSmall, isPivot,
														 iQ+1, iL+1, false,
														 false, total_PhysicalChannels_Strip[iQ]);
      
      int Xcoord = 0;

      l_Strip[iQ][iL].resize(p_NumConnChan_VMMid.first.size());
      
      for(unsigned int iNum=0; iNum < p_NumConnChan_VMMid.first.size(); iNum++){
	
	if (m_Verbose) std::cout << "FindLowNoiseChannels_wAbsRef -Strips: " << iNum << "no. of strip channels connected " << p_NumConnChan_VMMid.first[iNum] << std::endl;
	
	Xcoord+=p_NumConnChan_VMMid.first[iNum];
	
	if(m_Verbose) std::cout << "FindLowNoiseChannels_wAbsRef - Strips - Xcoord: " << Xcoord <<std::endl;
	
	if(iQ==0)
	  l_Strip[iQ][iL][iNum] = new TLine( Xcoord+0.5,0,Xcoord+0.5,Strip_maxYAxis);
	else if(iQ==1)
	  l_Strip[iQ][iL][iNum] = new TLine(total_PhysicalChannels_Strip[0]+Xcoord+0.5,0,total_PhysicalChannels_Strip[0]+Xcoord+0.5,Strip_maxYAxis);
	else if(iQ==2)
	  l_Strip[iQ][iL][iNum] = new TLine(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+Xcoord+0.5,0,total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+Xcoord+0.5,Strip_maxYAxis);
	
	l_Strip[iQ][iL][iNum]->SetLineColor(kRed);
	l_Strip[iQ][iL][iNum]->SetLineStyle(1);
        
	if(iNum == p_NumConnChan_VMMid.first.size()-1){
	  l_Strip[iQ][iL][iNum]->SetLineWidth(3);
	}
	else {
	  l_Strip[iQ][iL][iNum]->SetLineWidth(1);
	}
        
	l_Strip[iQ][iL][iNum]->Draw("same");
        
      }
      
    }
    
    S_line_median[0][iL] = new TLine(0.5, v_Strip_median_rms_perlayer_perQuad[0][iL].first, total_PhysicalChannels_Strip[0]+0.5, v_Strip_median_rms_perlayer_perQuad[0][iL].first);
    S_line_median[0][iL]->SetLineColor(kBlue);
    S_line_median[0][iL]->SetLineWidth(2.5);
    S_line_median[0][iL]->SetLineStyle(1);
    S_line_median[0][iL]->Draw("same");
    
    S_line_halfmedian[0][iL] = new TLine(0.5, 0.5*v_Strip_median_rms_perlayer_perQuad[0][iL].first, total_PhysicalChannels_Strip[0]+0.5, 0.5*v_Strip_median_rms_perlayer_perQuad[0][iL].first);
    S_line_halfmedian[0][iL]->SetLineColor(kBlue);
    S_line_halfmedian[0][iL]->SetLineWidth(1.5);
    S_line_halfmedian[0][iL]->SetLineStyle(1);
    S_line_halfmedian[0][iL]->Draw("same");
    
    S_line_twicemedian[0][iL] = new TLine(0.5, 2*v_Strip_median_rms_perlayer_perQuad[0][iL].first, total_PhysicalChannels_Strip[0]+0.5, 2*v_Strip_median_rms_perlayer_perQuad[0][iL].first);
    S_line_twicemedian[0][iL]->SetLineColor(kBlue);
    S_line_twicemedian[0][iL]->SetLineWidth(1.5);
    S_line_twicemedian[0][iL]->SetLineStyle(1);
    S_line_twicemedian[0][iL]->Draw("same");
    
    S_line_median[1][iL] = new TLine(total_PhysicalChannels_Strip[0]+0.5, v_Strip_median_rms_perlayer_perQuad[1][iL].first, total_PhysicalChannels_Strip[0] + total_PhysicalChannels_Strip[1]+0.5, v_Strip_median_rms_perlayer_perQuad[1][iL].first);
    S_line_median[1][iL]->SetLineColor(kBlue);
    S_line_median[1][iL]->SetLineWidth(2.5);
    S_line_median[1][iL]->SetLineStyle(1);
    S_line_median[1][iL]->Draw("same");
    
    S_line_halfmedian[1][iL] = new TLine(total_PhysicalChannels_Strip[0]+0.5, 0.5*v_Strip_median_rms_perlayer_perQuad[1][iL].first, total_PhysicalChannels_Strip[0] + total_PhysicalChannels_Strip[1] + 0.5, 0.5*v_Strip_median_rms_perlayer_perQuad[1][iL].first);
    S_line_halfmedian[1][iL]->SetLineColor(kBlue);
    S_line_halfmedian[1][iL]->SetLineWidth(1.5);
    S_line_halfmedian[1][iL]->SetLineStyle(1);
    S_line_halfmedian[1][iL]->Draw("same");
    
    S_line_twicemedian[1][iL] = new TLine(total_PhysicalChannels_Strip[0]+0.5, 2*v_Strip_median_rms_perlayer_perQuad[1][iL].first, total_PhysicalChannels_Strip[0] + total_PhysicalChannels_Strip[1] + 0.5, 2*v_Strip_median_rms_perlayer_perQuad[1][iL].first);
    S_line_twicemedian[1][iL]->SetLineColor(kBlue);
    S_line_twicemedian[1][iL]->SetLineWidth(1.5);
    S_line_twicemedian[1][iL]->SetLineStyle(1);
    S_line_twicemedian[1][iL]->Draw("same");
    
    S_line_median[2][iL] = new TLine(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+0.5, v_Strip_median_rms_perlayer_perQuad[2][iL].first, total_PhysicalChannels_Strip[0] + total_PhysicalChannels_Strip[1] + total_PhysicalChannels_Strip[2]+0.5, v_Strip_median_rms_perlayer_perQuad[2][iL].first);
    S_line_median[2][iL]->SetLineColor(kBlue);
    S_line_median[2][iL]->SetLineWidth(2.5);
    S_line_median[2][iL]->SetLineStyle(1);
    S_line_median[2][iL]->Draw("same");
    
    S_line_halfmedian[2][iL] = new TLine(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+0.5, 0.5*v_Strip_median_rms_perlayer_perQuad[2][iL].first, total_PhysicalChannels_Strip[0] + total_PhysicalChannels_Strip[1] + total_PhysicalChannels_Strip[2]+0.5, 0.5*v_Strip_median_rms_perlayer_perQuad[2][iL].first);
    S_line_halfmedian[2][iL]->SetLineColor(kBlue);
    S_line_halfmedian[2][iL]->SetLineWidth(1.5);
    S_line_halfmedian[2][iL]->SetLineStyle(1);
    S_line_halfmedian[2][iL]->Draw("same");
    
    S_line_twicemedian[2][iL] = new TLine(total_PhysicalChannels_Strip[0]+total_PhysicalChannels_Strip[1]+0.5, 2*v_Strip_median_rms_perlayer_perQuad[2][iL].first, total_PhysicalChannels_Strip[0] + total_PhysicalChannels_Strip[1] + total_PhysicalChannels_Strip[2]+0.5, 2*v_Strip_median_rms_perlayer_perQuad[2][iL].first);
    S_line_twicemedian[2][iL]->SetLineColor(kBlue);
    S_line_twicemedian[2][iL]->SetLineWidth(1.5);
    S_line_twicemedian[2][iL]->SetLineStyle(1);
    S_line_twicemedian[2][iL]->Draw("same");
    
    gPad->Update();
    
    t_Legend[0][iL][0] = new TLegend(0.2,0.73,0.3,0.78,"", "brNDC");
    if(isSmall && isPivot) t_Legend[0][iL][0]->AddEntry((TObject*)0,"QS1P","");
    else if(isSmall && !isPivot) t_Legend[0][iL][0]->AddEntry((TObject*)0,"QS1C","");
    else if(!isSmall && isPivot) t_Legend[0][iL][0]->AddEntry((TObject*)0,"QL1P","");
    else if(!isSmall && !isPivot) t_Legend[0][iL][0]->AddEntry((TObject*)0,"QL1C","");
    t_Legend[0][iL][0]->Draw("same");

    t_Legend[1][iL][0] = new TLegend(0.5,0.73,0.6,0.78,"", "brNDC");
    if(isSmall && isPivot) t_Legend[1][iL][0]->AddEntry((TObject*)0,"QS2P","");
    else if(isSmall && !isPivot) t_Legend[1][iL][0]->AddEntry((TObject*)0,"QS2C","");
    else if(!isSmall && isPivot) t_Legend[1][iL][0]->AddEntry((TObject*)0,"QL2P","");
    else if(!isSmall && !isPivot) t_Legend[1][iL][0]->AddEntry((TObject*)0,"QL2C","");
    t_Legend[1][iL][0]->Draw("same");

    t_Legend[2][iL][0] = new TLegend(0.8,0.73,0.9,0.78,"", "brNDC");
    if(isSmall && isPivot) t_Legend[2][iL][0]->AddEntry((TObject*)0,"QS3P","");
    else if(isSmall && !isPivot) t_Legend[2][iL][0]->AddEntry((TObject*)0,"QS3C","");
    else if(!isSmall && isPivot) t_Legend[2][iL][0]->AddEntry((TObject*)0,"QL3P","");
    else if(!isSmall && !isPivot) t_Legend[2][iL][0]->AddEntry((TObject*)0,"QL3C","");
    t_Legend[2][iL][0]->Draw("same");

    gPad->Update();
    
    Strip_leg[iL] = new TLegend(0.30, 0.80, 0.90, 0.90,"", "brNDC");
    if(iL==0) {
      Strip_leg[iL]->AddEntry(h_Strip_Stdev_perLayer_TH1[iL],"Strip- Layer 1 stdev on baseline ","l");
      if(isSmall) Strip_leg[iL]->AddEntry((TObject*)0,"VMMid-(Q1:7-0, Q2:7-2, Q3:7-3)","");
      else if(!isSmall) Strip_leg[iL]->AddEntry((TObject*)0,"VMMid-(Q1:7-0, Q2:7-2, Q3:7-2)","");
      //Strip_leg[iL]->AddEntry((TObject*)0,"Layer 1: VMMid decreases with Phys chan no.","");
    }
    else if(iL==1) {
      Strip_leg[iL]->AddEntry(h_Strip_Stdev_perLayer_TH1[iL],"Strip- Layer 2 stdev on baseline ","l");
      if(isSmall) Strip_leg[iL]->AddEntry((TObject*)0,"VMMid-(Q1:0-7, Q2:2-7, Q3:3-7)","");
      else if(!isSmall) Strip_leg[iL]->AddEntry((TObject*)0,"VMMid-(Q1:0-7, Q2:2-7, Q3:2-7)","");
      //Strip_leg[iL]->AddEntry((TObject*)0,"Layer 2: VMMid increases with Phys chan no.","");
    }
    else if(iL==2) {
      Strip_leg[iL]->AddEntry(h_Strip_Stdev_perLayer_TH1[iL],"Strip- Layer 3 stdev on baseline ","l");
      if(isSmall) Strip_leg[iL]->AddEntry((TObject*)0,"VMMid-(Q1:0-7, Q2:2-7, Q3:3-7)","");
      else if(!isSmall) Strip_leg[iL]->AddEntry((TObject*)0,"VMMid-(Q1:0-7, Q2:2-7, Q3:2-7)","");
      //Strip_leg[iL]->AddEntry((TObject*)0,"Layer 3: VMMid increases with Phys chan no.","");
    }
    else if(iL==3) {
      Strip_leg[iL]->AddEntry(h_Strip_Stdev_perLayer_TH1[iL],"Strip- Layer 4 stdev on baseline ","l");
      if(isSmall) Strip_leg[iL]->AddEntry((TObject*)0,"VMMid-(Q1:7-0, Q2:7-2, Q3:7-3)","");
      else if(isSmall) Strip_leg[iL]->AddEntry((TObject*)0,"VMMid-(Q1:7-0, Q2:7-2, Q3:7-2)","");
      //Strip_leg[iL]->AddEntry((TObject*)0,"Layer 4: VMMid decreases with Phys chan no.","");
    }
    
    Strip_leg[iL]->Draw("same");
    
    gPad->Modified();
    
    //###################################################################################//
    /////////////////////// Wires histograms ///////////////////////////////////////////// 
    //###################################################################################// 
    
    c_Wire_Stdev_perLayer[iL]->cd();
    
    std::string name_Wire_h_1D = "Wire_h_1D_Layer"+std::to_string(iL+1);
    
    Wire_h_1D_perLayer[iL] = h_Wire_Stdev_perLayer[iL]->ProfileX(name_Wire_h_1D.c_str(),      1, -1, "");
    
    Wire_h_1D_perLayer[iL]->Draw("L");
    
    double Wire_maxYAxis = Wire_h_1D_perLayer[iL]->GetBinContent(Wire_h_1D_perLayer[iL]->GetMaximumBin())*1.5;
    
    Wire_h_1D_perLayer[iL]->GetYaxis()->SetRangeUser(0,Wire_maxYAxis);
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetTitleSize(0.045);
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetLabelSize(0.045);
    Wire_h_1D_perLayer[iL]->GetYaxis()->SetTitleSize(0.045);
    Wire_h_1D_perLayer[iL]->GetYaxis()->SetLabelSize(0.045);
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetTitleOffset(1);
    Wire_h_1D_perLayer[iL]->GetYaxis()->SetTitleOffset(1);
    Wire_h_1D_perLayer[iL]->SetMarkerSize(0.75);
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetTitle("Wire Physical Channel No.");
    Wire_h_1D_perLayer[iL]->GetYaxis()->SetTitle("Stdev on baseline ADC Sample [mV]");
    Wire_h_1D_perLayer[iL]->SetMarkerColor(kBlue);
    Wire_h_1D_perLayer[iL]->SetLineColor(kBlue);
    Wire_h_1D_perLayer[iL]->SetLineWidth(3);
    Wire_h_1D_perLayer[iL]->SetMarkerStyle(kFullSquare);

    std::string Wire_Q1, Wire_Q2, Wire_Q3;

    Wire_Q1 = std::to_string(total_PhysicalChannels_Wire_S[0][iL]);
    Wire_Q2 = std::to_string(total_PhysicalChannels_Wire_S[1][iL]);
    Wire_Q3 = std::to_string(total_PhysicalChannels_Wire_S[2][iL]);

    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBit(TAxis::kLabelsHori);
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(1,"1");

    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL],Wire_Q1.c_str());
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL], Wire_Q2.c_str());
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+total_PhysicalChannels_Wire_S[2][iL], Wire_Q3.c_str());
    
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(10,"10");

    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+10,"10");
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+20,"20");

    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+10,"10");
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+20,"20");
    Wire_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+30,"30");
    
    W_myline1[iL] = new TLine(total_PhysicalChannels_Wire_S[0][iL]+0.5,0,total_PhysicalChannels_Wire_S[0][iL]+0.5,Wire_maxYAxis);
    W_myline1[iL]->SetLineColor(kRed);
    W_myline1[iL]->SetLineWidth(3);
    W_myline1[iL]->SetLineStyle(1);
    W_myline1[iL]->Draw("same");
    
    W_myline2[iL] = new TLine(total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+0.5,0,total_PhysicalChannels_Wire_S[0][iL]+total_PhysicalChannels_Wire_S[1][iL]+0.5,Wire_maxYAxis);
    W_myline2[iL]->SetLineColor(kRed);
    W_myline2[iL]->SetLineWidth(3);
    W_myline2[iL]->SetLineStyle(1);
    W_myline2[iL]->Draw("same");
    
    Wire_leg[iL] = new TLegend(0.60, 0.80, 0.90, 0.90,"", "brNDC");
    if(iL==0) Wire_leg[iL]->AddEntry(Wire_h_1D_perLayer[iL],"Wire- Layer 1 stdev on baseline ","l");
    else if(iL==1) Wire_leg[iL]->AddEntry(Wire_h_1D_perLayer[iL],"Wire- Layer 2 stdev on baseline ","l");
    else if(iL==2) Wire_leg[iL]->AddEntry(Wire_h_1D_perLayer[iL],"Wire- Layer 3 stdev on baseline ","l");
    else if(iL==3) Wire_leg[iL]->AddEntry(Wire_h_1D_perLayer[iL],"Wire- Layer 4 stdev on baseline ","l");
    
    Wire_leg[iL]->Draw();
    
    gPad->Modified();
    
    //###################################################################################//
    /////////////////////// Pads histograms /////////////////////////////////////////////
    //###################################################################################// 
    
    if( (v_Pad_lowNoiseChannels[iL][0].size() + v_Pad_lowNoiseChannels[iL][1].size() + v_Pad_lowNoiseChannels[iL][2].size()) == 0) {
      c_Pad_Stdev_perLayer[iL]->SetFillColor(kGreen);
      c_Pad_Stdev_perLayer[iL]->SetFrameFillColor(10);
      PadLayer_ChannelAlivenessRating[iL]=Perfect;
    }
    else if( (v_Pad_lowNoiseChannels[iL][0].size() + v_Pad_lowNoiseChannels[iL][1].size() + v_Pad_lowNoiseChannels[iL][2].size()) <= 2) {
      c_Pad_Stdev_perLayer[iL]->SetFillColor(kYellow);
      c_Pad_Stdev_perLayer[iL]->SetFrameFillColor(10);
      PadLayer_ChannelAlivenessRating[iL]=Ok;
    }
    else {
      c_Pad_Stdev_perLayer[iL]->SetFillColor(kRed);
      c_Pad_Stdev_perLayer[iL]->SetFrameFillColor(10);
      PadLayer_ChannelAlivenessRating[iL]=Problematic;
    }
    
    c_Pad_Stdev_perLayer[iL]->cd();
    
    std::string name_Pad_h_1D = "Pad_h_1D_Layer"+std::to_string(iL+1);
    
    Pad_h_1D_perLayer[iL] = h_Pad_Stdev_perLayer[iL]->ProfileX(name_Pad_h_1D.c_str(),      1, -1, "");
    
    double Pad_maxYAxis = Pad_h_1D_perLayer[iL]->GetBinContent(Pad_h_1D_perLayer[iL]->GetMaximumBin())*1.5;
    
    Pad_h_1D_perLayer[iL]->Draw("L");
    
    Pad_h_1D_perLayer[iL]->GetYaxis()->SetRangeUser(0,Pad_maxYAxis);
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetTitleSize(0.045);
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetLabelSize(0.045);
    Pad_h_1D_perLayer[iL]->GetYaxis()->SetTitleSize(0.045);
    Pad_h_1D_perLayer[iL]->GetYaxis()->SetLabelSize(0.045);
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetTitleOffset(1);
    Pad_h_1D_perLayer[iL]->GetYaxis()->SetTitleOffset(1);
    Pad_h_1D_perLayer[iL]->SetMarkerSize(0.75);
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetTitle("Pad Physical Channel No.");
    Pad_h_1D_perLayer[iL]->GetYaxis()->SetTitle("Stdev on baseline ADC Sample [mV]");
    Pad_h_1D_perLayer[iL]->SetMarkerColor(kBlue);
    Pad_h_1D_perLayer[iL]->SetLineColor(kBlue);
    Pad_h_1D_perLayer[iL]->SetLineWidth(3);
    Pad_h_1D_perLayer[iL]->SetMarkerStyle(kFullSquare);

    std::string Pad_Q1, Pad_Q2, Pad_Q3;

    Pad_Q1 = std::to_string(total_PhysicalChannels_Pad_S[0][iL]);
    Pad_Q2 = std::to_string(total_PhysicalChannels_Pad_S[1][iL]);
    Pad_Q3 = std::to_string(total_PhysicalChannels_Pad_S[2][iL]);
    
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetBit(TAxis::kLabelsHori);
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(1,"1");
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL], Pad_Q1.c_str());
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL], Pad_Q2.c_str());
    Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+total_PhysicalChannels_Pad_S[2][iL], Pad_Q3.c_str());
    
    if(iL==0 || iL==1){

      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(20,"20");
      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(40,"40");
      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(60,"60");      
      if(!isSmall){
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(80,"80");
      }
      
      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+20,"20");
      if(isSmall == true && isPivot == false){
        Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+40,"40");
      }
      else if(isSmall == false && isPivot == true){
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+40,"40");
      }
      else if(isSmall == false && isPivot == false){
	
      } 

      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+20,"20");
      if(isSmall == true && isPivot == false){
	
      }
      else if(isSmall == false && isPivot == true){
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+40,"40");
      } 
      else if(isSmall == false && isPivot == false){
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+40,"40");
      }

    }

    else if(iL==2 || iL==3){
      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(20,"20");
      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(40,"40");
      if(isSmall == true && isPivot == false){
        Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(60,"60");
      }
      else if(isSmall == false && isPivot == true){
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(80,"80");
      }
      else if(isSmall == false && isPivot == false){

      }

      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+20,"20");
      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+40,"40");
      if(isSmall == true && isPivot == false){
	
      }
      else if(isSmall == false && isPivot == true){
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+60,"60");
      }
      else if(isSmall == false && isPivot == false){
	
      }

      Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+20,"20");
      if(isSmall == true && isPivot == false){
	
      }
      else if(isSmall == false && isPivot == true){
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+40,"40");
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+60,"60");
      }
      else if(isSmall == false && isPivot == false){
	Pad_h_1D_perLayer[iL]->GetXaxis()->SetBinLabel(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+40,"40");
      }
      

    }

    //================== Pads - Drawing TBoxes for  each VMM ==============================// 
    
    /*for(int iQ=0; iQ<total_quads; iQ++){
      
      std::pair <std::vector<int>, std::vector<int>> p_NumConnChan_VMMid = m_ABMap->NumChan_perVmm_PhysConnected(isSmall, isPivot,
      iQ+1, iL+1, true,
      true, total_PhysicalChannels_Pad_S[iQ][iL]);
      
      box_Pad[iQ][iL].resize(p_NumConnChan_VMMid.first.size());
      
      int X1_coord = 0, X2_coord = 0;
      int Y1_coord = 0, Y2_coord = Pad_maxYAxis;
      
      for(unsigned int iNum=0; iNum < p_NumConnChan_VMMid.first.size(); iNum++){
      
      X1_coord = X2_coord;
      X2_coord += p_NumConnChan_VMMid.first[iNum];
      
      box_Pad[iQ][iL][iNum] = new TBox(X1_coord, Y1_coord, X2_coord, Y2_coord);
      if(p_NumConnChan_VMMid.second[iNum] == 1){
      //box_Pad[iQ][iL][iNum]->SetFillStyle(3002);
      box_Pad[iQ][iL][iNum]->SetFillColorAlpha(kBlue, 0.35);
      }
      else if(p_NumConnChan_VMMid.second[iNum] == 2){
      //box_Pad[iQ][iL][iNum]->SetFillStyle(0);
      box_Pad[iQ][iL][iNum]->SetFillColorAlpha(10, 0);
      }
      
      //box_Pad[iQ][iL][iNum]->Draw();
      
      }
      
      }*/
    
    //===================== Drawing the histogram ================//
    
    //Pad_h_1D_perLayer[iL]->Draw("L SAME");
    
    //================== Pads - Drawing vertical TLines ==============================//
    
    std::string legendName = "Starting VMM-";
    
    for(int iQ=0; iQ<total_quads; iQ++){
      
      //std::cout << "Quad: " << iQ+1 << " Layer: " << iL+1 << std::endl;
      
      std::pair <std::vector<int>, std::vector<int>> p_NumConnChan_VMMid = m_ABMap->NumChan_perVmm_PhysConnected(isSmall, isPivot,
														 iQ+1, iL+1, true,
														 true, total_PhysicalChannels_Pad_S[iQ][iL]);
      
      //std::cout << "total_PhysicalChannels_Pad_S[iQ][iL]: " << total_PhysicalChannels_Pad_S[iQ][iL] << std::endl;
      
      l_Pad[iQ][iL].resize(p_NumConnChan_VMMid.first.size());

      int Xcoord = 0;
      
      for(unsigned int iNum=0; iNum < p_NumConnChan_VMMid.first.size(); iNum++){
	
	//std::cout << iNum << "no. of pad channels connected " << p_NumConnChan_VMMid.first[iNum] << std::endl;
	
	Xcoord+=p_NumConnChan_VMMid.first[iNum];
	
	//std::cout << "Xcoord: "<< Xcoord <<std::endl;
	
	if(iQ==0)
	  l_Pad[iQ][iL][iNum] = new TLine(Xcoord+0.5,0,Xcoord+0.5,Pad_maxYAxis);
	else if(iQ==1)
	  l_Pad[iQ][iL][iNum] = new TLine(total_PhysicalChannels_Pad_S[0][iL]+Xcoord+0.5,0,total_PhysicalChannels_Pad_S[0][iL]+Xcoord+0.5,Pad_maxYAxis);
	else if(iQ==2)
	  l_Pad[iQ][iL][iNum] = new TLine(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+Xcoord+0.5,0,total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+Xcoord+0.5,Pad_maxYAxis);
	
	l_Pad[iQ][iL][iNum]->SetLineColor(kRed);
	l_Pad[iQ][iL][iNum]->SetLineStyle(1);
	
	if(iNum == p_NumConnChan_VMMid.first.size()-1){
	  l_Pad[iQ][iL][iNum]->SetLineWidth(3);
	}
	else{
	  l_Pad[iQ][iL][iNum]->SetLineWidth(1);
	}
	
	l_Pad[iQ][iL][iNum]->Draw("same");
	
      }
      legendName = legendName + " Q" + std::to_string(iQ+1) + ":" + std::to_string(p_NumConnChan_VMMid.second[0]) ;
    }
    
    P_line_median[0][iL] = new TLine(0.5, v_Pad_median_rms_perlayer_perQuad[0][iL].first, total_PhysicalChannels_Pad_S[0][iL]+0.5, v_Pad_median_rms_perlayer_perQuad[0][iL].first);
    P_line_median[0][iL]->SetLineColor(kBlue);
    P_line_median[0][iL]->SetLineWidth(2.5);
    P_line_median[0][iL]->SetLineStyle(1);
    P_line_median[0][iL]->Draw("same");
    
    P_line_halfmedian[0][iL] = new TLine(0.5, 0.5*v_Pad_median_rms_perlayer_perQuad[0][iL].first, total_PhysicalChannels_Pad_S[0][iL]+0.5, 0.5*v_Pad_median_rms_perlayer_perQuad[0][iL].first);
    P_line_halfmedian[0][iL]->SetLineColor(kBlue);
    P_line_halfmedian[0][iL]->SetLineWidth(1.5);
    P_line_halfmedian[0][iL]->SetLineStyle(1);
    P_line_halfmedian[0][iL]->Draw("same");
    
    P_line_twicemedian[0][iL] = new TLine(0.5, 2*v_Pad_median_rms_perlayer_perQuad[0][iL].first, total_PhysicalChannels_Pad_S[0][iL]+0.5, 2*v_Pad_median_rms_perlayer_perQuad[0][iL].first);
    P_line_twicemedian[0][iL]->SetLineColor(kBlue);
    P_line_twicemedian[0][iL]->SetLineWidth(1.5);
    P_line_twicemedian[0][iL]->SetLineStyle(1);
    P_line_twicemedian[0][iL]->Draw("same");
    
    P_line_median[1][iL] = new TLine(total_PhysicalChannels_Pad_S[0][iL]+0.5, v_Pad_median_rms_perlayer_perQuad[1][iL].first, total_PhysicalChannels_Pad_S[0][iL] + total_PhysicalChannels_Pad_S[1][iL]+0.5, v_Pad_median_rms_perlayer_perQuad[1][iL].first);
    P_line_median[1][iL]->SetLineColor(kBlue);
    P_line_median[1][iL]->SetLineWidth(2.5);
    P_line_median[1][iL]->SetLineStyle(1);
    P_line_median[1][iL]->Draw("same");
    
    P_line_halfmedian[1][iL] = new TLine(total_PhysicalChannels_Pad_S[0][iL]+0.5, 0.5*v_Pad_median_rms_perlayer_perQuad[1][iL].first, total_PhysicalChannels_Pad_S[0][iL] + total_PhysicalChannels_Pad_S[1][iL] + 0.5, 0.5*v_Pad_median_rms_perlayer_perQuad[1][iL].first);
    P_line_halfmedian[1][iL]->SetLineColor(kBlue);
    P_line_halfmedian[1][iL]->SetLineWidth(1.5);
    P_line_halfmedian[1][iL]->SetLineStyle(1);
    P_line_halfmedian[1][iL]->Draw("same");
    
    P_line_twicemedian[1][iL] = new TLine(total_PhysicalChannels_Pad_S[0][iL]+0.5, 2*v_Pad_median_rms_perlayer_perQuad[1][iL].first, total_PhysicalChannels_Pad_S[0][iL] + total_PhysicalChannels_Pad_S[1][iL] + 0.5, 2*v_Pad_median_rms_perlayer_perQuad[1][iL].first);
    P_line_twicemedian[1][iL]->SetLineColor(kBlue);
    P_line_twicemedian[1][iL]->SetLineWidth(1.5);
    P_line_twicemedian[1][iL]->SetLineStyle(1);
    P_line_twicemedian[1][iL]->Draw("same");
    
    P_line_median[2][iL] = new TLine(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+0.5, v_Pad_median_rms_perlayer_perQuad[2][iL].first, total_PhysicalChannels_Pad_S[0][iL] + total_PhysicalChannels_Pad_S[1][iL] + total_PhysicalChannels_Pad_S[2][iL]+0.5, v_Pad_median_rms_perlayer_perQuad[2][iL].first);
    P_line_median[2][iL]->SetLineColor(kBlue);
    P_line_median[2][iL]->SetLineWidth(2.5);
    P_line_median[2][iL]->SetLineStyle(1);
    P_line_median[2][iL]->Draw("same");
    
    P_line_halfmedian[2][iL] = new TLine(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+0.5, 0.5*v_Pad_median_rms_perlayer_perQuad[2][iL].first, total_PhysicalChannels_Pad_S[0][iL] + total_PhysicalChannels_Pad_S[1][iL] + total_PhysicalChannels_Pad_S[2][iL]+0.5, 0.5*v_Pad_median_rms_perlayer_perQuad[2][iL].first);
    P_line_halfmedian[2][iL]->SetLineColor(kBlue);
    P_line_halfmedian[2][iL]->SetLineWidth(1.5);
    P_line_halfmedian[2][iL]->SetLineStyle(1);
    P_line_halfmedian[2][iL]->Draw("same");
    
    P_line_twicemedian[2][iL] = new TLine(total_PhysicalChannels_Pad_S[0][iL]+total_PhysicalChannels_Pad_S[1][iL]+0.5, 2*v_Pad_median_rms_perlayer_perQuad[2][iL].first, total_PhysicalChannels_Pad_S[0][iL] + total_PhysicalChannels_Pad_S[1][iL] + total_PhysicalChannels_Pad_S[2][iL]+0.5, 2*v_Pad_median_rms_perlayer_perQuad[2][iL].first);
    P_line_twicemedian[2][iL]->SetLineColor(kBlue);
    P_line_twicemedian[2][iL]->SetLineWidth(1.5);
    P_line_twicemedian[2][iL]->SetLineStyle(1);
    P_line_twicemedian[2][iL]->Draw("same");
    
    gPad->Update();

    t_Legend[0][iL][1] = new TLegend(0.2,0.73,0.3,0.78);
    if(isSmall && isPivot) t_Legend[0][iL][1]->AddEntry((TObject*)0,"QS1P","");
    else if(isSmall && !isPivot) t_Legend[0][iL][1]->AddEntry((TObject*)0,"QS1C","");
    else if(!isSmall && isPivot) t_Legend[0][iL][1]->AddEntry((TObject*)0,"QL1P","");
    else if(!isSmall && !isPivot) t_Legend[0][iL][1]->AddEntry((TObject*)0,"QL1C","");
    t_Legend[0][iL][1]->Draw("same");
    
    t_Legend[1][iL][1] = new TLegend(0.56,0.73,0.66,0.78);
    if(isSmall && isPivot) t_Legend[1][iL][1]->AddEntry((TObject*)0,"QS2P","");
    else if(isSmall && !isPivot) t_Legend[1][iL][1]->AddEntry((TObject*)0,"QS2C","");
    else if(!isSmall && isPivot) t_Legend[1][iL][1]->AddEntry((TObject*)0,"QL2P","");
    else if(!isSmall && !isPivot) t_Legend[1][iL][1]->AddEntry((TObject*)0,"QL2C","");
    t_Legend[1][iL][1]->Draw("same");

    t_Legend[2][iL][1] = new TLegend(0.8,0.73,0.9,0.78);
    if(isSmall && isPivot) t_Legend[2][iL][1]->AddEntry((TObject*)0,"QS3P","");
    else if(isSmall && !isPivot) t_Legend[2][iL][1]->AddEntry((TObject*)0,"QS3C","");
    else if(!isSmall && isPivot) t_Legend[2][iL][1]->AddEntry((TObject*)0,"QL3P","");
    else if(!isSmall && !isPivot) t_Legend[2][iL][1]->AddEntry((TObject*)0,"QL3C","");
    t_Legend[2][iL][1]->Draw("same");

    gPad->Update();

    Pad_leg[iL] = new TLegend(0.10, 0.80, 0.90, 0.90,"", "brNDC");
    if(iL==0) {
      Pad_leg[iL]->AddEntry(Pad_h_1D_perLayer[iL],"Pad- Layer 1 stdev on baseline ","l");
      Pad_leg[iL]->AddEntry((TObject*)0,legendName.c_str(),"");
      Pad_leg[iL]->AddEntry((TObject*)0,"VMMid switches btn 1 & 2 - from left to right at each vertical line","");
    }
    else if(iL==1) {
      Pad_leg[iL]->AddEntry(Pad_h_1D_perLayer[iL],"Pad- Layer 2 stdev on baseline ","l");
      Pad_leg[iL]->AddEntry((TObject*)0,legendName.c_str(),"");
      Pad_leg[iL]->AddEntry((TObject*)0,"VMMid switches btn 1 & 2 - from left to right at each vertical line","");
    }
    else if(iL==2) {
      Pad_leg[iL]->AddEntry(Pad_h_1D_perLayer[iL],"Pad- Layer 3 stdev on baseline ","l");
      Pad_leg[iL]->AddEntry((TObject*)0,legendName.c_str(),"");
      Pad_leg[iL]->AddEntry((TObject*)0,"VMMid switches btn 1 & 2 - from left to right at each vertical line","");
    }
    else if(iL==3) {
      Pad_leg[iL]->AddEntry(Pad_h_1D_perLayer[iL],"Pad- Layer 4 stdev on baseline ","l");
      Pad_leg[iL]->AddEntry((TObject*)0,legendName.c_str(),"");
      Pad_leg[iL]->AddEntry((TObject*)0,"VMMid switches btn 1 & 2 - from left to right at each vertical line","");
    }
    
    Pad_leg[iL]->Draw("same");
    
    gPad->Modified();
    
  }
  
  // baseline ttree plots (Not the summary)
  
  for(int iQ=0; iQ<total_quads; iQ++){
    
    for(int iL=0; iL<total_layers; iL++){
      
      for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
	
	std::string FEBType;
	int numVMMs = -1;
	
	if(iispFEB==0) {FEBType = "sFEB"; numVMMs=total_vmms_per_sFEB;}
	else if(iispFEB==1) {FEBType = "pFEB"; numVMMs=total_vmms_per_pFEB;}
	else std::cout<<"Invalid value for ispFEB"<<std::endl;
	
	c_Stdev_perFEB_ElecChan[iQ][iL][iispFEB]->cd();
	h_Stdev_perFEB_ElecChan[iQ][iL][iispFEB]->Draw();
	h_Stdev_perFEB_ElecChan[iQ][iL][iispFEB]->GetXaxis()->SetTitle("Electronic Channel No.");
	h_Stdev_perFEB_ElecChan[iQ][iL][iispFEB]->GetYaxis()->SetTitle("Stdev on baseline ADC Sample");
	
	for(int iVMM=0; iVMM<numVMMs; iVMM++){
	  
	  c_baseline_query_vmm[iQ][iL][iispFEB][iVMM]->cd();
	  h_baseline_query_vmm[iQ][iL][iispFEB][iVMM]->Draw("COLZ");
	  h_baseline_query_vmm[iQ][iL][iispFEB][iVMM]->GetYaxis()->SetTitle("Stdev on baseline ADC Sample");
	  h_baseline_query_vmm[iQ][iL][iispFEB][iVMM]->GetXaxis()->SetTitle("Query No.");
	  
	  for(int iChan=0; iChan<total_chans_pervmm; iChan++){
	    
	    c_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan]->cd();
	    h_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan]->Draw();
	    h_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan]->GetYaxis()->SetTitle("Count ");
            h_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan]->GetXaxis()->SetTitle("Baseline Pdo ADC value");
            
	    c_baseline_mV[iQ][iL][iispFEB][iVMM][iChan]->cd();
	    h_baseline_mV[iQ][iL][iispFEB][iVMM][iChan]->Draw();
	    h_baseline_mV[iQ][iL][iispFEB][iVMM][iChan]->GetYaxis()->SetTitle("Count ");
            h_baseline_mV[iQ][iL][iispFEB][iVMM][iChan]->GetXaxis()->SetTitle("Baseline Pdo [mV]");
            
	    c_baseline_query[iQ][iL][iispFEB][iVMM][iChan]->cd();
	    h_baseline_query[iQ][iL][iispFEB][iVMM][iChan]->Draw("COLZ");
	    h_baseline_query[iQ][iL][iispFEB][iVMM][iChan]->GetYaxis()->SetTitle("Stdev on baseline ADC Sample");
	    h_baseline_query[iQ][iL][iispFEB][iVMM][iChan]->GetXaxis()->SetTitle("Query No.");
	    
	  }
	}
      }
    }
  }
  
}

void AnalyzeBlThres::PrintLowHighNoiseChannels(){

    m_strOutput_Phys << "SUMMARY OF PROBLEMATIC CHANNELS: Considering Physical channel baseline-stdev plots " << std::endl;
    
    m_strOutput_Phys << std::endl;

    m_strOutput_Phys << "SorL" << " " << "PorC" << " " << "Layer" << " " << "Quadruplet" << " " << "Strip_LowNoise_PhysChannels" << " " << "Pad_LowNoise_PhysChannels" << " " << "sFEB_LowNoiseChannels" << " " << "pFEB_LowNoiseChannels(pad)" << " " << "Strip_HighNoise_PhysChannels" << " " << "Pad_HighNoise_PhysChannels" << " " << "sFEB_HighNoiseChannels" << " " << "pFEB_HighNoiseChannels(pad)" << std::endl;
    
    for(int iL=0; iL<total_layers; iL++){
        
        for(int iQ=0; iQ<total_quads; iQ++){
            
            m_strOutput_Phys << s_SorL << " " << s_PorC << " " << iL+1 << " " << iQ+1 << " ";

            if(v_Strip_lowNoiseChannels[iL][iQ].size() == 0) m_strOutput_Phys << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_Strip_lowNoiseChannels[iL][iQ].size(); iChan++){
                    
                    m_strOutput_Phys << v_Strip_lowNoiseChannels[iL][iQ][iChan] << "," ;
                    
                }
            }
            
            m_strOutput_Phys << " " ;
            
            if(v_Pad_lowNoiseChannels[iL][iQ].size() == 0) m_strOutput_Phys << "-------- ";
            
            else{
                for(unsigned int iChan=0; iChan < v_Pad_lowNoiseChannels[iL][iQ].size(); iChan++){
                    
                    m_strOutput_Phys << v_Pad_lowNoiseChannels[iL][iQ][iChan] << ",";
                    
                }
            }
            
            m_strOutput_Phys << " " ;

            if(v_lowNoiseElecChannels[iL][iQ][0].size() == 0) m_strOutput_Phys << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_lowNoiseElecChannels[iL][iQ][0].size(); iChan++){

                    m_strOutput_Phys << v_lowNoiseElecChannels[iL][iQ][0][iChan].first << "(" << v_lowNoiseElecChannels[iL][iQ][0][iChan].second << "),";

                }
            }

            m_strOutput_Phys << " " ;

            if(v_lowNoiseElecChannels[iL][iQ][1].size() == 0) m_strOutput_Phys << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_lowNoiseElecChannels[iL][iQ][1].size(); iChan++){

                    m_strOutput_Phys << v_lowNoiseElecChannels[iL][iQ][1][iChan].first << "(" << v_lowNoiseElecChannels[iL][iQ][1][iChan].second << "),";

                }
            }

            m_strOutput_Phys << " " ;

            if(v_Strip_highNoiseChannels[iL][iQ].size() == 0) m_strOutput_Phys << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_Strip_highNoiseChannels[iL][iQ].size(); iChan++){

                    m_strOutput_Phys << v_Strip_highNoiseChannels[iL][iQ][iChan] << "," ;

                }
            }

            m_strOutput_Phys << " " ;

            if(v_Pad_highNoiseChannels[iL][iQ].size() == 0) m_strOutput_Phys << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_Pad_highNoiseChannels[iL][iQ].size(); iChan++){

                    m_strOutput_Phys << v_Pad_highNoiseChannels[iL][iQ][iChan] << ",";

                }
            }

            m_strOutput_Phys << " " ;

            if(v_highNoiseElecChannels[iL][iQ][0].size() == 0) m_strOutput_Phys << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_highNoiseElecChannels[iL][iQ][0].size(); iChan++){

                    m_strOutput_Phys << v_highNoiseElecChannels[iL][iQ][0][iChan].first << "(" << v_highNoiseElecChannels[iL][iQ][0][iChan].second << "),";

                }
            }

            m_strOutput_Phys << " " ;

            if(v_highNoiseElecChannels[iL][iQ][1].size() == 0) m_strOutput_Phys << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_highNoiseElecChannels[iL][iQ][1].size(); iChan++){

                    m_strOutput_Phys << v_highNoiseElecChannels[iL][iQ][1][iChan].first << "(" << v_highNoiseElecChannels[iL][iQ][1][iChan].second << "),";

                }
            }

            m_strOutput_Phys << std::endl;

        }
        
    }
    
    /*for(int iL=0; iL<total_layers; iL++){
        
        m_strOutput_Phys << "Strip_Channel_Aliveness_Rating_Layer_" << iL+1 << ": " << StripLayer_ChannelAlivenessRating[iL] << std::endl;
        m_strOutput_Phys << "Pad_Channel_Aliveness_Rating_Layer_" << iL+1 << ": " << PadLayer_ChannelAlivenessRating[iL] << std::endl;

        }*/

    m_strOutput_Phys << "#Strips_LowNoise: " << v_Strip_lowNoiseChannels[0][0].size()+v_Strip_lowNoiseChannels[0][1].size()+v_Strip_lowNoiseChannels[0][2].size()+v_Strip_lowNoiseChannels[1][0].size()+v_Strip_lowNoiseChannels[1][1].size()+v_Strip_lowNoiseChannels[1][2].size()+v_Strip_lowNoiseChannels[2][0].size()+v_Strip_lowNoiseChannels[2][1].size()+v_Strip_lowNoiseChannels[2][2].size()+v_Strip_lowNoiseChannels[3][0].size()+v_Strip_lowNoiseChannels[3][1].size()+v_Strip_lowNoiseChannels[3][2].size() << std::endl;
    
    m_strOutput_Phys << "#Pads_LowNoise: " << v_Pad_lowNoiseChannels[0][0].size()+v_Pad_lowNoiseChannels[0][1].size()+v_Pad_lowNoiseChannels[0][2].size()+v_Pad_lowNoiseChannels[1][0].size()+v_Pad_lowNoiseChannels[1][1].size()+v_Pad_lowNoiseChannels[1][2].size()+v_Pad_lowNoiseChannels[2][0].size()+v_Pad_lowNoiseChannels[2][1].size()+v_Pad_lowNoiseChannels[2][2].size()+v_Pad_lowNoiseChannels[3][0].size()+v_Pad_lowNoiseChannels[3][1].size()+v_Pad_lowNoiseChannels[3][2].size() << std::endl;

    m_strOutput_Phys << "#Strips_HighNoise: " << v_Strip_highNoiseChannels[0][0].size()+v_Strip_highNoiseChannels[0][1].size()+v_Strip_highNoiseChannels[0][2].size()+v_Strip_highNoiseChannels[1][0].size()+v_Strip_highNoiseChannels[1][1].size()+v_Strip_highNoiseChannels[1][2].size()+v_Strip_highNoiseChannels[2][0].size()+v_Strip_highNoiseChannels[2][1].size()+v_Strip_highNoiseChannels[2][2].size()+v_Strip_highNoiseChannels[3][0].size()+v_Strip_highNoiseChannels[3][1].size()+v_Strip_highNoiseChannels[3][2].size() << std::endl;

    m_strOutput_Phys << "#Pads_HighNoise: " << v_Pad_highNoiseChannels[0][0].size()+v_Pad_highNoiseChannels[0][1].size()+v_Pad_highNoiseChannels[0][2].size()+v_Pad_highNoiseChannels[1][0].size()+v_Pad_highNoiseChannels[1][1].size()+v_Pad_highNoiseChannels[1][2].size()+v_Pad_highNoiseChannels[2][0].size()+v_Pad_highNoiseChannels[2][1].size()+v_Pad_highNoiseChannels[2][2].size()+v_Pad_highNoiseChannels[3][0].size()+v_Pad_highNoiseChannels[3][1].size()+v_Pad_highNoiseChannels[3][2].size() << std::endl;


    m_strOutput_Elec << "SUMMARY OF PROBLEMATIC CHANNELS: Considering Electronic channel baseline-stdev plots " << std::endl;

    m_strOutput_Elec << std::endl;

    m_strOutput_Elec << "SorL" << " " << "PorC" << " " << "Layer" << " " << "Quadruplet" << " " << "Strip_LowNoise_PhysChannels" << " " << "Pad_LowNoise_PhysChannels" << " " << "sFEB_LowNoiseChannels" <<" " << "pFEB_LowNoiseChannels(pad)" << std::endl;

    for(int iL=0; iL<total_layers; iL++){

        for(int iQ=0; iQ<total_quads; iQ++){

            m_strOutput_Elec << s_SorL << " " << s_PorC << " " << iL+1 << " " << iQ+1 << " ";

            if(v_Strip_lowNoiseChannels_UnConnChanRef[iL][iQ].size() == 0) m_strOutput_Elec << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_Strip_lowNoiseChannels_UnConnChanRef[iL][iQ].size(); iChan++){

                    m_strOutput_Elec << v_Strip_lowNoiseChannels_UnConnChanRef[iL][iQ][iChan] << "," ;

                }
            }

            m_strOutput_Elec << " " ;

            if(v_Pad_lowNoiseChannels_UnConnChanRef[iL][iQ].size() == 0) m_strOutput_Elec << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_Pad_lowNoiseChannels_UnConnChanRef[iL][iQ].size(); iChan++){

                    m_strOutput_Elec << v_Pad_lowNoiseChannels_UnConnChanRef[iL][iQ][iChan] << ",";

                }
            }

            m_strOutput_Elec << " " ;
            
            if(v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][0].size() == 0) m_strOutput_Elec << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][0].size(); iChan++){

                    m_strOutput_Elec << v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][0][iChan].first << "(" << v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][0][iChan].second << "),";

                }
            }

            m_strOutput_Elec << " " ;

            if(v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][1].size() == 0) m_strOutput_Elec << "-------- ";

            else{
                for(unsigned int iChan=0; iChan < v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][1].size(); iChan++){

                    m_strOutput_Elec << v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][1][iChan].first << "(" << v_lowNoiseElecChannels_UnConnChanRef[iL][iQ][1][iChan].second << "),";

                }
            }
            
            m_strOutput_Elec << std::endl;

        }

    }
    
    m_strOutput_Elec << "#Strips_LowNoise: " << v_Strip_lowNoiseChannels_UnConnChanRef[0][0].size()+v_Strip_lowNoiseChannels_UnConnChanRef[0][1].size()+v_Strip_lowNoiseChannels_UnConnChanRef[0][2].size()+v_Strip_lowNoiseChannels_UnConnChanRef[1][0].size()+v_Strip_lowNoiseChannels_UnConnChanRef[1][1].size()+v_Strip_lowNoiseChannels_UnConnChanRef[1][2].size()+v_Strip_lowNoiseChannels_UnConnChanRef[2][0].size()+v_Strip_lowNoiseChannels_UnConnChanRef[2][1].size()+v_Strip_lowNoiseChannels_UnConnChanRef[2][2].size()+v_Strip_lowNoiseChannels_UnConnChanRef[3][0].size()+v_Strip_lowNoiseChannels_UnConnChanRef[3][1].size()+v_Strip_lowNoiseChannels_UnConnChanRef[3][2].size() << std::endl;

    m_strOutput_Elec << "#Pads_LowNoise: " << v_Pad_lowNoiseChannels_UnConnChanRef[0][0].size()+v_Pad_lowNoiseChannels_UnConnChanRef[0][1].size()+v_Pad_lowNoiseChannels_UnConnChanRef[0][2].size()+v_Pad_lowNoiseChannels_UnConnChanRef[1][0].size()+v_Pad_lowNoiseChannels_UnConnChanRef[1][1].size()+v_Pad_lowNoiseChannels_UnConnChanRef[1][2].size()+v_Pad_lowNoiseChannels_UnConnChanRef[2][0].size()+v_Pad_lowNoiseChannels_UnConnChanRef[2][1].size()+v_Pad_lowNoiseChannels_UnConnChanRef[2][2].size()+v_Pad_lowNoiseChannels_UnConnChanRef[3][0].size()+ v_Pad_lowNoiseChannels_UnConnChanRef[3][1].size()+v_Pad_lowNoiseChannels_UnConnChanRef[3][2].size() << std::endl;

    m_strOutput_Phys_AbsRef << "SUMMARY OF PROBLEMATIC CHANNELS: Considering Physical channel baseline-stdev plots with Absolute Reference" << std::endl;
    
    m_strOutput_Phys_AbsRef << std::endl;
    
    m_strOutput_Phys_AbsRef << "SorL" << " " << "PorC" << " " << "Layer" << " " << "Quadruplet" << " " << "Strip_LowNoise_PhysChannels" << " " << "sFEB_LowNoiseChannels" << std::endl;

    for(int iL=0; iL<total_layers; iL++){
      
      for(int iQ=0; iQ<total_quads; iQ++){
	
	m_strOutput_Phys_AbsRef << s_SorL << " " << s_PorC << " " << iL+1 << " " << iQ+1 << " ";
	
	if(v_Strip_lowNoiseChannels_AbsRef[iL][iQ].size() == 0) m_strOutput_Phys_AbsRef << "-------- ";
	
	else{
	  for(unsigned int iChan=0; iChan < v_Strip_lowNoiseChannels_AbsRef[iL][iQ].size(); iChan++){
	    
	    m_strOutput_Phys_AbsRef << v_Strip_lowNoiseChannels_AbsRef[iL][iQ][iChan] << "," ;
	    
	  }
	}
	
	m_strOutput_Phys_AbsRef << " " ;
	
	if(v_lowNoiseElecChannels_AbsRef[iL][iQ][0].size() == 0) m_strOutput_Phys_AbsRef << "-------- ";
	
	else{
	  for(unsigned int iChan=0; iChan < v_lowNoiseElecChannels_AbsRef[iL][iQ][0].size(); iChan++){
	    
	    m_strOutput_Phys_AbsRef << v_lowNoiseElecChannels_AbsRef[iL][iQ][0][iChan].first << "(" << v_lowNoiseElecChannels_AbsRef[iL][iQ][0][iChan].second << "),";
	    
	  }
	}
	
	m_strOutput_Phys_AbsRef << " " ;

        if(v_Pad_lowNoiseChannels_AbsRef[iL][iQ].size() == 0) m_strOutput_Phys_AbsRef << "-------- ";

        else{
          for(unsigned int iChan=0; iChan < v_Pad_lowNoiseChannels_AbsRef[iL][iQ].size(); iChan++){

            m_strOutput_Phys_AbsRef << v_Pad_lowNoiseChannels_AbsRef[iL][iQ][iChan] << "," ;

          }
        }

        m_strOutput_Phys_AbsRef << " " ;

        if(v_lowNoiseElecChannels_AbsRef[iL][iQ][1].size() == 0) m_strOutput_Phys_AbsRef << "-------- ";

        else{
          for(unsigned int iChan=0; iChan < v_lowNoiseElecChannels_AbsRef[iL][iQ][1].size(); iChan++){

            m_strOutput_Phys_AbsRef << v_lowNoiseElecChannels_AbsRef[iL][iQ][1][iChan].first << "(" << v_lowNoiseElecChannels_AbsRef[iL][iQ][1][iChan].second << "),";

          }

	}

	m_strOutput_Phys_AbsRef << std::endl;
	
      }
      
    }

}

AnalyzeBlThres::~AnalyzeBlThres(){
    
  oFile_web->cd();
    
  for(int iL=0; iL<total_layers; iL++){
    c_Strip_Stdev_perLayer[iL]->Write("",TObject::kOverwrite);
    c_Wire_Stdev_perLayer[iL]->Write("",TObject::kOverwrite);
    c_Pad_Stdev_perLayer[iL]->Write("",TObject::kOverwrite);
    
    delete c_Strip_Stdev_perLayer[iL];
    delete c_Wire_Stdev_perLayer[iL];
    delete c_Pad_Stdev_perLayer[iL];
    
    delete h_Strip_Stdev_perLayer[iL];
    delete h_Wire_Stdev_perLayer[iL];
    delete h_Pad_Stdev_perLayer[iL];
    
    delete Strip_leg[iL];
    delete Wire_leg[iL];
    delete Pad_leg[iL];
    
    delete W_myline1[iL];
    delete W_myline2[iL];
    
    for(int iQ=0; iQ<total_quads; iQ++){
      
      delete S_line_median[iQ][iL];
      delete S_line_halfmedian[iQ][iL];
      delete S_line_twicemedian[iQ][iL];
      
      delete P_line_median[iQ][iL];
      delete P_line_halfmedian[iQ][iL];
      delete P_line_twicemedian[iQ][iL];
      
      for(unsigned int iFEB=0; iFEB < total_types_FEBs; iFEB++){
	
	delete t_Legend[iQ][iL][iFEB];
	
      }
      
      for(unsigned int iLine=0; iLine < l_Strip[iQ][iL].size(); iLine++){
	
	delete l_Strip[iQ][iL][iLine];
        
      }
      
      for(unsigned int iLine=0; iLine < l_Pad[iQ][iL].size(); iLine++){
	
	delete l_Pad[iQ][iL][iLine];
        
      }
      
      for(unsigned int iBox=0; iBox < box_Pad[iQ][iL].size(); iBox++){
	delete box_Pad[iQ][iL][iBox];
      }
      
    }
  }
  
  oFile_extra->cd();
  
  for(int iQ=0; iQ<total_quads; iQ++){

    for(int iL=0; iL<total_layers; iL++){
      
      delete h_Strip_Stdev_perLayer_perQuad[iQ][iL];
      delete h_Pad_Stdev_perLayer_perQuad[iQ][iL];
      
      for(int iispFEB=0; iispFEB<total_types_FEBs; iispFEB++){
	
	std::string FEBType;
	int numVMMs = -1;
	
	if(iispFEB==0) {FEBType = "sFEB"; numVMMs=total_vmms_per_sFEB;}
	else if(iispFEB==1) {FEBType = "pFEB"; numVMMs=total_vmms_per_pFEB;}
	else std::cout<<"Invalid value for ispFEB"<<std::endl;
	
	oFile_extra->cd(Stdev_perFEBDirName[iQ][iL][iispFEB].c_str());
	h_Stdev_perFEB_ElecChan[iQ][iL][iispFEB]->Write("",TObject::kOverwrite);
	c_Stdev_perFEB_ElecChan[iQ][iL][iispFEB]->Write("",TObject::kOverwrite);
        
	delete h_Stdev_perFEB_ElecChan[iQ][iL][iispFEB];
	delete c_Stdev_perFEB_ElecChan[iQ][iL][iispFEB];
	
	for(int iVMM=0; iVMM<numVMMs; iVMM++){
	  
	  oFile_extra->cd(VMMDirName[iQ][iL][iispFEB][iVMM].c_str());
	  
	  oFile_extra->cd(Baseline_queryDirName[iQ][iL][iispFEB][iVMM].c_str());
	  h_baseline_query_vmm[iQ][iL][iispFEB][iVMM]->Write("",TObject::kOverwrite);
	  c_baseline_query_vmm[iQ][iL][iispFEB][iVMM]->Write("",TObject::kOverwrite);
	  
	  delete h_baseline_query_vmm[iQ][iL][iispFEB][iVMM];
	  delete c_baseline_query_vmm[iQ][iL][iispFEB][iVMM];
	  
	  for(int iChan=0; iChan<total_chans_pervmm; iChan++){
	    
	    oFile_extra->cd(Baseline_pdoDirName[iQ][iL][iispFEB][iVMM].c_str());
	    h_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan]->Write("",TObject::kOverwrite);
	    c_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan]->Write("",TObject::kOverwrite);
	    
	    oFile_extra->cd(Baseline_mVDirName[iQ][iL][iispFEB][iVMM].c_str());
	    h_baseline_mV[iQ][iL][iispFEB][iVMM][iChan]->Write("",TObject::kOverwrite);
	    c_baseline_mV[iQ][iL][iispFEB][iVMM][iChan]->Write("",TObject::kOverwrite);
	    
	    oFile_extra->cd(Baseline_queryDirName[iQ][iL][iispFEB][iVMM].c_str());
	    h_baseline_query[iQ][iL][iispFEB][iVMM][iChan]->Write("",TObject::kOverwrite);
	    c_baseline_query[iQ][iL][iispFEB][iVMM][iChan]->Write("",TObject::kOverwrite);
	    
	    delete h_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan];
	    delete h_baseline_mV[iQ][iL][iispFEB][iVMM][iChan];
	    delete h_baseline_query[iQ][iL][iispFEB][iVMM][iChan];
	    
	    delete c_baseline_pdo[iQ][iL][iispFEB][iVMM][iChan];
	    delete c_baseline_mV[iQ][iL][iispFEB][iVMM][iChan];
	    delete c_baseline_query[iQ][iL][iispFEB][iVMM][iChan];
	    
	  }
          
	}
	
      }
      
    }
    
  }
  
  oFile_web->Close();
  oFile_extra->Close();
  
  delete oFile_web;
  delete oFile_extra;
  
  delete baseline_Summary->GetCurrentFile();
  delete baseline->GetCurrentFile();
  
  std::cout<<"Destructor Called "<<std::endl;
}
