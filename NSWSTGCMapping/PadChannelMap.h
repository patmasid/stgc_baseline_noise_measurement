// @Siyuan.Sun@cern.ch
#ifndef PadChannelMap_H
#define PadChannelMap_H

#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include <vector>
#include <utility>
#include <map>

#include <iterator>

class PadChannelMap {

 public :
  PadChannelMap();
  virtual ~PadChannelMap(){};

  std::map<std::string, std::map<std::pair<int,int>,std::pair<int,int>> > DetectorPadMapping;
  std::map<std::string, int>                                              DetectorNPadsPerRow;
  std::map<std::string, int>                                              DetectorNPadsTotal;

  std::stringstream m_sx;
  bool verbose;

  void Init();
  void Clear();
  
  void SetVerbose(bool v);

  bool SetNPadChannelMap( bool isSmall, bool isPivot,
			  int quadNum, int layerNum,
			  int n_tot_pads, int n_pads_per_row );

  bool SetPadChannelMap( int ith_vmm,  int ith_vmm_chan,
			 bool isSmall, bool isPivot,
			 int quadNum, int layerNum, 
			 int padNumConstruction, int padNumAssembly );
  
  bool ReturnNPadPerRow( bool isSmall, bool isPivot,
			 int quadNum, int layerNum,
			 int &n_pads_per_row ) ;

  bool ReturnNPadsTotal( bool isSmall, bool isPivot,
			 int quadNum, int layerNum,
			 int &n_tot_pads ) ;

  bool ReturnPadChannelNumber( int ith_vmm,  int ith_vmm_chan,
			       bool isSmall, bool isPivot,
			       int quadNum, int layerNum, 
			       int &padNumConstruction, int &padNumAssembly ) ;
  
  bool ReturnPadChannelNumber( int ith_vmm,  int ith_vmm_chan,
                               bool isSmall, bool isPivot,
                               int quadNum, int layerNum, 
			       int &padXConstruction, int &padYConstruction,
			       int &padXAssembly,     int &padYAssembly,
			       int &n_pads_per_row ) ;


  bool ReturnElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
						    bool isSmall, bool isPivot,
						    int quadNum, int layerNum, 
						    int padXConstruction, int padYConstruction,
						    int &padXAssembly, int &padYAssembly) ;

  bool ReturnElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
						bool isSmall, bool isPivot,
						int quadNum, int layerNum,
						int &padXConstruction, int &padYConstruction,
						int padXAssembly, int padYAssembly) ;


  bool ReturnElectronicsChannelNumber_Construction( int &ith_vmm,  int &ith_vmm_chan,
						    bool isSmall, bool isPivot,
						    int quadNum, int layerNum, 
						    int padNumConstruction,
						    int &padNumAssembly) ;
  
  bool ReturnElectronicsChannelNumber_Assembly( int &ith_vmm,  int &ith_vmm_chan,
						bool isSmall, bool isPivot,
						int quadNum, int layerNum,
						int &padNumConstruction,
						int padNumAssembly) ;

};

#endif
