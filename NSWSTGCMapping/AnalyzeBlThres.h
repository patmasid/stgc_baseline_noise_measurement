#ifndef ANALYZEBLTHRES_H
#define ANALYZEBLTHRES_H

#include <iostream>
#include <fstream>
#include <cmath>
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <vector>
#include "TGraph.h"
#include <fstream>
#include "TTree.h"
#include "TCanvas.h"
#include <TLegend.h>
#include "ABMapping.h"
#include "TLine.h"
#include "TPaveText.h"

class AnalyzeBlThres {

 public:
    
    AnalyzeBlThres(const char *inputFileSumm, const char *inputFile, const char *inputHistFile, const char* outFileName_webpage, const char *outFileName_extra, const char* outputFilePath, const char* SorL, const char* PorC);
    ~AnalyzeBlThres();
    
    void     SetVerbose(bool v);
    void     InitMapHistograms(); //, const char* VMMidMapFile);                                                              
    void     CreateHistDir();
    void     EventLoop_Summary();
    void     EventLoop();
    void     SaveHistograms();
    void     FindLowHighNoiseChannels_wPhysicalChanPlot();
    void     FindLowNoiseChannels_wUnconnChanRef();
    void     FindLowNoiseChannels_wAbsRef();
    void     PrintLowHighNoiseChannels();

    bool m_Verbose;

    bool isSmall, isPivot;
    std::string s_SorL, s_PorC;

    TTree * baseline_Summary;
    TTree * baseline;

    //Summary of Baseline plots 

    std::vector<TH2F *> h_Strip_Stdev_perLayer;
    std::vector<TH1F *> h_Strip_Stdev_perLayer_TH1;
    std::vector<TCanvas *> c_Strip_Stdev_perLayer;

    std::vector<TH2F *> h_Wire_Stdev_perLayer;
    std::vector<TCanvas *> c_Wire_Stdev_perLayer;
    
    std::vector<TH2F *> h_Pad_Stdev_perLayer;
    std::vector<TCanvas *> c_Pad_Stdev_perLayer;

    std::vector<std::vector<TH2F *>> h_Strip_Stdev_perLayer_perQuad;
    std::vector<std::vector<TH2F *>> h_Pad_Stdev_perLayer_perQuad;

    std::vector<std::vector<std::vector<TH2F *>>> h_Stdev_perFEB_ElecChan;
    std::vector<std::vector<std::vector<TCanvas *>>> c_Stdev_perFEB_ElecChan;

    std::vector<TProfile *> Strip_h_1D_perLayer;
    std::vector<TProfile *> Wire_h_1D_perLayer;
    std::vector<TProfile *> Pad_h_1D_perLayer;

    //================== Baseline plots ==========================//  
    
    std::vector<std::vector<std::vector<std::vector<std::vector< TH1F * >>>>> h_baseline_pdo;
    std::vector<std::vector<std::vector<std::vector<std::vector< TCanvas * >>>>> c_baseline_pdo;
    
    std::vector<std::vector<std::vector<std::vector<std::vector< TH1F * >>>>> h_baseline_mV;
    std::vector<std::vector<std::vector<std::vector<std::vector< TCanvas * >>>>> c_baseline_mV;
    
    std::vector<std::vector<std::vector<std::vector<std::vector< TH2F * >>>>> h_baseline_query;
    std::vector<std::vector<std::vector<std::vector<std::vector< TCanvas * >>>>> c_baseline_query;

    std::vector<std::vector<std::vector<std::vector< TH2F * >>>> h_baseline_query_vmm;
    std::vector<std::vector<std::vector<std::vector< TCanvas * >>>> c_baseline_query_vmm;

    //=================== Directories

    std::vector<std::string> quadDirName;
    std::vector< std::vector<std::string>> LayerDirName;
    std::vector< std::vector< std::vector <std::string>>> febTypeDirName;
    std::vector< std::vector< std::vector <std::string>>> Stdev_perFEBDirName;
    std::vector< std::vector< std::vector <std::vector<std::string>>>> VMMDirName;
    std::vector< std::vector< std::vector <std::vector<std::string>>>> Baseline_pdoDirName, Baseline_mVDirName, Baseline_queryDirName;

    std::vector<TLegend *> Strip_leg, Wire_leg, Pad_leg;

    int total_quads;
    int total_layers;
    int total_types_FEBs;
    int total_vmms_per_sFEB;
    int total_vmms_per_pFEB;
    int total_chans_pervmm;
    
    std::vector<int> total_PhysicalChannels_Strip;

    std::vector<std::vector<int>> total_PhysicalChannels_Wire_S;
    std::vector<std::vector<int>> total_PhysicalChannels_Wire_L;
    std::vector<std::vector<int>> total_PhysicalChannels_Pad_S;
    std::vector<std::vector<int>> total_PhysicalChannels_Pad_L;

    ABMapping *m_ABMap;

    char boardname[100];
    double vmmidnum;
    double channelidnum;
    double meannum;
    double stdevnum;
    double mediannum;

    char boardname_bl[100];
    double vmmidnum_bl;
    double channelidnum_bl;
    double tpdac_bl;
    double random1_bl;
    double random2_bl;
    double baseline_bl;

    TFile *oFile_web, *oFile_extra, *iHistFile;

    std::vector< std::vector <TLine *> > S_line_median;
    std::vector< std::vector <TLine *> > S_line_halfmedian;
    std::vector< std::vector <TLine *> > S_line_twicemedian;

    std::vector< TLine *> W_myline1;
    std::vector< TLine *> W_myline2;

    std::vector< std::vector <TLine *> > P_line_median;
    std::vector< std::vector <TLine *> > P_line_halfmedian;
    std::vector< std::vector <TLine *> > P_line_twicemedian;

    // For tagging histograms 
    
    std::vector< std::pair<double, double>> v_Strip_median_rms_perlayer;
    std::vector< std::vector< std::pair<double, double> >> v_Strip_median_rms_perlayer_perQuad;
    std::vector< std::pair<double, double>> v_Pad_median_rms_perlayer;
    std::vector< std::vector< std::pair<double, double> >> v_Pad_median_rms_perlayer_perQuad;

    std::vector< std::vector< std::vector <int>>> v_Strip_lowNoiseChannels;
    std::vector< std::vector< std::vector <int>>> v_Pad_lowNoiseChannels;
    std::vector< std::vector< std::vector < std::vector<std::pair<int,int> >>>> v_lowNoiseElecChannels;

    std::vector< std::vector< std::vector <int>>> v_Strip_lowNoiseChannels_AbsRef;
    std::vector< std::vector< std::vector <int>>> v_Pad_lowNoiseChannels_AbsRef;
    std::vector< std::vector< std::vector < std::vector<std::pair<int,int> >>>> v_lowNoiseElecChannels_AbsRef;

    //======== Problematic channels using the unconnected channels as reference

    std::vector< std::vector< std::vector <int>>> v_Strip_lowNoiseChannels_UnConnChanRef;
    std::vector< std::vector< std::vector <int>>> v_Pad_lowNoiseChannels_UnConnChanRef;
    std::vector< std::vector< std::vector < std::vector<std::pair<int,int> >>>> v_lowNoiseElecChannels_UnConnChanRef;
    
    std::vector< std::vector< std::vector <int>>> v_Strip_highNoiseChannels;
    std::vector< std::vector< std::vector <int>>> v_Pad_highNoiseChannels;
    std::vector< std::vector< std::vector < std::vector<std::pair<int,int> >>>> v_highNoiseElecChannels;

    std::vector<std::string> StripLayer_ChannelAlivenessRating;
    std::vector<std::string> PadLayer_ChannelAlivenessRating;

    int num_Strip_lowNoiseChannels, num_Pad_lowNoiseChannels, num_Strip_highNoiseChannels, num_Pad_highNoiseChannels;
    
    std::string outputFilePath_;

    std::ofstream m_strOutput_Phys, m_strOutput_Elec, m_strOutput_Phys_AbsRef, m_strOutput_baslines, m_strOutput_baslines_PhysStripPad;

    //===================== Lines on the plots =========================//
    
    std::vector< std::vector <std::vector <TLine *>>> l_Strip;
    std::vector< std::vector <std::vector <TLine *>>> l_Pad;

    std::vector< std::vector <std::vector < TLegend *> > > t_Legend;

    //===================== TBoxes on pad plots =========================// 

    std::vector< std::vector <std::vector <TBox *>>> box_Pad;

};

#endif
