tdaq_package() #This will make a package called NSWConfiguration

# for linking the libraries dynamically
SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/$ENV{CMTCONFIG}/lib")

message(STATUS "mapping file base directory is: " ${CMAKE_CURRENT_SOURCE_DIR}/data)

find_package( ROOT REQUIRED COMPONENTS Core Hist Tree RIO Physics MathCore Gui Gpad Graf)

include_directories(. ) 

set(SOURCE_FILES
    src/ABMapping.cxx
    src/PadChannelMap.cxx
    src/StripChannelMap.cxx
    src/WireChannelMap.cxx
    src/AnalyzeBlThres.cxx
)

tdaq_add_library( NSWSTGCMapping ${SOURCE_FILES}
    INCLUDE_DIRECTORIES ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES}
)

tdaq_add_executable(analyzeHit app/run.cxx ${SOURCE_FILES}
    LINK_LIBRARIES NSWSTGCMapping 
)
