# stgc_baseline_noise_measurement

Using the electronic channel to physical channel mapping for the data of stdev on the baseline measurement to understand the noise and connectivity

```bash
mkdir stgc_baseline_noiseMeasurement

cd stgc_baseline_noiseMeasurement

git clone <url >

source ./stgc_baselineStdev_noise/setupEnv.sh

printf "cmake_minimum_required(VERSION 3.4.3)\nfind_package(TDAQ)\ninclude(CTest)\ntdaq_project(NSWDAQ 1.0.0 USES tdaq 8.2.0)\n" > CMakeLists.txt
```

### This script is run on the output of the script https://gitlab.cern.ch/atlas-muon-nsw-daq/NSWConfiguration/tree/LTPPulsingAndBaselineScripts_180. which is the baseline scan of the wedges. example output can be found here: 

/afs/cern.ch/work/s/stgcic/public/sTGC_quick_and_dirty_baselines/trimmers_sTGC/2020_02_12_16h14m49s_DebugNoBaseline_20MNIWSAP00005

# Running the executable: 

/afs/cern.ch/work/s/stgcic/public/stgc_baselines/x86_64-centos7-gcc8-opt/stgc_baseline_noise_measurement/analyzeHit (8 arguments

input summary-txt file (summary_baselines.txt)

input baselines-txt file (baselines.txt)

input histogram-file (trimmers_2020*.root, the root output created after baseline scan)

output web-file name (e.g. baseline_stdev_eachlayer_strip_pad.root )

output extra-file name (e.g. extra_baseline_plots*.root)

output file path (with '/' at the end)

SmallOrLarge ('S' or 'L')

PivotOrConfirm ('P' or 'C')

# About the outputs:

***About the files inside each of this run folder after generating the output:***
1. ***baseline_outside_150_200mV.txt OR baseline_outside_150_200mV_2.txt (Both are the same)*** : -  channels with baseline mean value outside the range 150 to 200 mV.
2. ***baseline_stdev_eachlayer_strip_pad.root*** : - RMS on baseline versus physical channel number (per layer) plots. Also found on the webpage
3. ***extra_baseline_stdev_eachlayer_strip_pad.root*** :- per channel plots of baseline pdo distributions with some more detailed plots
4. ***baselines.txt*** :- baseline values per channel per measurement
5. ***summary_baselines.txt*** :- baseline mean median and stdev per channel
6. ***output_ProbChan_wPhysicalChanPlots.txt*** :- Finding median for each Quad and layer. The low noise channels are those which are found below the half of the median for that quad and layer. The high noise channels have the noise level above 2 times the median.
7. ***output_ProbChan_wPhysicalChanPlots_AbsRef.txt*** :- Any noise level below 0.5 mV (for strips) and below 0.8 mV (for pads) is considered to be low noise.
8. ***output_ProbChan_wUnConnChanRef.txt*** :- Taking the reference of the unconnected channels. Anything below the maximum noise level of the unconnected channels is consered to be low noise. However, this method has problem in the case where there are high noise unconnected channels.
9. ***baseline_outside_150_200mV.txt/baseline_outside_150_200mV_2.txt***:- These are the high/low baseline channels (outside the range 150 to 200 mV).
10. ***baseline_outside_150_200mV_PhysStripPad.txt***:- These are the high/low baseline channels which are physically connected to strip or pad (outside the range 150 to 200 mV).
11. ***trimmers_Run_00XXX_2020_XX_XX_XXhXXmXXs_x30.root/trimmers_Run_00XXX_2020_XX_XX_XXhXXmXXs.root***:- per board plots of baseline or stdev on baseline.
12. ***Only for threshold / trimming scan - trimmers_x30.json***:- json file with the trimmer values for threshold 30 DAC above the baseline.