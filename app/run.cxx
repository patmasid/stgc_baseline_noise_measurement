#define run_cxx

#include <iostream>
#include <vector>
#include <cstring>
#include "NSWSTGCMapping/AnalyzeBlThres.h"
#include <cmath>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <algorithm>
//#include <TH2.h>                                                                                                                                                                                          
#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <array>

#include <iomanip>
#include "THStack.h"
#include <math.h>

#include "TApplication.h"
#include <TSystem.h>



using namespace std;

int main(int argc, char* argv[])
{

    if (argc < 9) {
        cerr << "Please give 8 arguments, " << "input summary-txt file, input baselines-txt file, input histogram-file, output web-file name, output extra-file name, output file path, SmallOrLarge, PivotOrConfirm" <<endl;
        return -1;
    }
    const char *inputFileSumm    = argv[1];
    const char *inputFile        = argv[2];
    const char *inputHistFile    = argv[3];
    const char *outFileName_webpage      = argv[4];
    const char *outFileName_extra      = argv[5];
    const char *outFilePath      = argv[6];
    const char *SmallOrLarge     = argv[7];
    const char *PivotOrConfirm   = argv[8];

    std::string outFile_web = outFilePath;
    std::string outFile_extra = outFilePath;
    std::string outName_web = outFileName_webpage;
    outFile_web = outFile_web + outFileName_webpage;
    outFile_extra = outFile_extra + outFileName_extra;
    
    AnalyzeBlThres ana(inputFileSumm, inputFile, inputHistFile, outFile_web.c_str(), outFile_extra.c_str(), outFilePath, SmallOrLarge, PivotOrConfirm);

    ana.InitMapHistograms(); 
    ana.CreateHistDir();
    ana.EventLoop_Summary(); 
    ana.EventLoop();
    ana.FindLowHighNoiseChannels_wPhysicalChanPlot();
    ana.FindLowNoiseChannels_wUnconnChanRef();
    ana.FindLowNoiseChannels_wAbsRef();
    ana.SaveHistograms();
    ana.PrintLowHighNoiseChannels();
    return 0;
}
